package com.cafe24.allin2.model;

public class DriverModel {

	private String shopCode;
	private String shopName;
	private String phone;
	private String addressTail;
	private String creditYn;
	private String deliverlyYn;
	private String menuYn;
	private String couponYn;
	private String deliverlyArea;
	private String shopHours;
	private String smallLogImgFileName;
	private String menuImgFileName;
	
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddressTail() {
		return addressTail;
	}
	public void setAddressTail(String addressTail) {
		this.addressTail = addressTail;
	}
	public String getCreditYn() {
		return creditYn;
	}
	public void setCreditYn(String creditYn) {
		this.creditYn = creditYn;
	}
	public String getDeliverlyYn() {
		return deliverlyYn;
	}
	public void setDeliverlyYn(String deliverlyYn) {
		this.deliverlyYn = deliverlyYn;
	}
	public String getMenuYn() {
		return menuYn;
	}
	public void setMenuYn(String menuYn) {
		this.menuYn = menuYn;
	}
	public String getCouponYn() {
		return couponYn;
	}
	public void setCouponYn(String couponYn) {
		this.couponYn = couponYn;
	}
	public String getDeliverlyArea() {
		return deliverlyArea;
	}
	public void setDeliverlyArea(String deliverlyArea) {
		this.deliverlyArea = deliverlyArea;
	}
	public String getShopHours() {
		return shopHours;
	}
	public void setShopHours(String shopHours) {
		this.shopHours = shopHours;
	}
	public String getSmallLogImgFileName() {
		return smallLogImgFileName;
	}
	public void setSmallLogImgFileName(String smallLogImgFileName) {
		this.smallLogImgFileName = smallLogImgFileName;
	}
	public String getMenuImgFileName() {
		return menuImgFileName;
	}
	public void setMenuImgFileName(String menuImgFileName) {
		this.menuImgFileName = menuImgFileName;
	}

}
