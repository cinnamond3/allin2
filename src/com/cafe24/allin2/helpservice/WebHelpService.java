package com.cafe24.allin2.helpservice;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

public class WebHelpService {
	
	Logger logger = Logger.getLogger(WebHelpService.class);

	public Map<String,String> getSearchMap(HttpServletRequest request) {
		Map<String, String> searchMap = new HashMap<String, String>();
		
		if(request.getParameter("type") != null){
			searchMap.put("type", request.getParameter("type").trim());

			if(request.getParameter("keyword") != null){
				searchMap.put("keyword", request.getParameter("keyword").trim());
			}
		}
		
		if(request.getParameter("type1") != null){
			searchMap.put("type1", request.getParameter("type1").trim());

			if(request.getParameter("keyword1") != null){
				searchMap.put("keyword1", request.getParameter("keyword1").trim());
			}
		}
		return searchMap;
	}
	
	public boolean isSearch(Map<String, String> searchMap) {
		return searchMap.get("keyword") != null && !searchMap.get("keyword").equals("")
				|| searchMap.get("keyword1") != null && !searchMap.get("keyword1").equals("");
	}
	
	// A method for Creating page html code
	public StringBuffer getPageHtml(int currentPage, int totalNum, int showArticleLimit, int showPageLimit, Map<String,String> searchMap) {
		StringBuffer pageHtml = new StringBuffer();
		int startPage = 0;
		int lastPage = 0;
		
		// expression page variables
		startPage = ((currentPage-1) / showPageLimit) * showPageLimit + 1;
		lastPage = startPage + showPageLimit - 1;
		
		if(lastPage > totalNum / showArticleLimit) {
			lastPage = (totalNum / showArticleLimit) + 1;
		}
		//
		
		// create page html code
		// if: when no search	
		if(searchMap.get("keyword") ==null && searchMap.get("keyword1") == null ){				
			if(currentPage == 1){
				pageHtml.append("<span>");
			} else {
				int page = (currentPage-showPageLimit) > 1 ? currentPage-showPageLimit : 1 ;
				pageHtml.append("<span><a href=\"list.do?page=" + page + "\"><이전></a>&nbsp;&nbsp;");
			}
			
			for(int i = startPage ; i <= lastPage ; i++) {
				if(i == currentPage){
					pageHtml.append(".&nbsp;<strong>");
					pageHtml.append("<a href=\"list.do?page=" +i + "\" class=\"page\">" + i + "</a>");
					pageHtml.append("&nbsp;</strong>");
				} else {
					pageHtml.append(".&nbsp;<a href=\"list.do?page=" +i + "\" class=\"page\">" + i + "</a>&nbsp;");
				}
				
			}
			if(currentPage == lastPage){
				pageHtml.append(".</span>");
			} else {
				int page = (currentPage+showPageLimit) < (totalNum / showArticleLimit) + 1 ? currentPage+showPageLimit : (totalNum / showArticleLimit) + 1 ;
				pageHtml.append(".&nbsp;&nbsp;<a href=\"list.do?page=" + page + "\"><다음></a></span>");
			}
		//
		// else: when search
		} else {
			
			String searchCondition = "&type=" + searchMap.get("type") + "&keyword=" + searchMap.get("keyword") 
					+ "&type1=" + searchMap.get("type1") + "&keyword1=" + searchMap.get("keyword1");
			
			if(currentPage == 1){
				pageHtml.append("<span>");
			} else {
				int page = (currentPage-showPageLimit) > 1 ? currentPage-showPageLimit : 1 ;
				pageHtml.append("<span><a href=\"list.do?page=" + page + searchCondition + "\"><이전></a>&nbsp;&nbsp;");
			}
			
			for(int i = startPage ; i <= lastPage ; i++) {
				if(i == currentPage){
					pageHtml.append(".&nbsp;<strong>");
					pageHtml.append("<a href=\"list.do?page=" + i + searchCondition + "\" class=\"page\">" + i + "</a>&nbsp;");
					pageHtml.append("&nbsp;</strong>");
				} else {
					pageHtml.append(".&nbsp;<a href=\"list.do?page=" + i + searchCondition + "\" class=\"page\">" + i + "</a>&nbsp;");
				}
				
			}
			if(currentPage == lastPage){
				pageHtml.append("</span>");
			} else {
				int page = (currentPage+showPageLimit) < (totalNum / showArticleLimit) + 1 ? currentPage+showPageLimit : (totalNum / showArticleLimit) + 1 ;
				pageHtml.append(".&nbsp;&nbsp;<a href=\"list.do?page=" + page + searchCondition + "\"><다음></a></span>");
			}
		}
		//		
		return pageHtml;
	}
	
	public void createNewUploadFile(MultipartHttpServletRequest request, String path, MultipartFile newFile, String newFileName) {
		// create new upload file
		File newUploadFile = new File(getUploadFileRealPath(request, path) + newFileName);
		
		// when file exists as same name
		if(newUploadFile.exists()){
			newFileName  = new Date().getTime() + newFileName ;
			newUploadFile = new File(getUploadFileRealPath(request, path) + newFileName );
		}
		
		try {
			newFile.transferTo(newUploadFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteOriginalFile(HttpServletRequest request, String path, String orgFileName) {
		if(orgFileName != null && !orgFileName.equals("")){
			// remove uploaded file
			File removeFile = new File(getUploadFileRealPath(request, path)  + orgFileName);
			removeFile.delete();
			//
		}
	}

	public String getUploadFileRealPath(HttpServletRequest request, String path) {
		String realPath = request.getSession().getServletContext().getRealPath(path)+"/";
		logger.debug(realPath);
		return realPath;
	}
	
	public String getUploadFilePath(HttpServletRequest request, String uploadDir) {
		int port = request.getServerPort();
		String strPort = "";
		if (port != 80) {
			strPort = ":" + String.valueOf(port);
		}
		
		return request.getScheme() + "://" + request.getServerName() + strPort + "/" + uploadDir;
	}
}
