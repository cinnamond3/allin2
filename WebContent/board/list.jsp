<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>게시판 글 목록 보기</title>
<link href="<%=request.getContextPath()%>/css/board.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript">
<!--
	function selectedOptionCheck(){
		$("#type > option[value=<%=request.getParameter("type")%>]").attr("selected", "true");
	}
	
	function view(index){
	    location.href = "view.do?idx="+index;
	}

	function moveAction(where){
		switch (where) {
		case 1:
			location.href = "write.do";
			break;
		
		case 2:
			location.href = "list.do";
			break;
		}
	}
//-->
</script>
</head>
<body onload="selectedOptionCheck()">
<div class="wrapper">
	<div class="nav">
		<jsp:include page="/common/menu.jsp"></jsp:include>
	</div>
	<div class="header">
		<form action="list.do" method="get">		
			<select id="type" name="type">
				<option value="subject">제목</option>
				<option value="content">내용</option>
				<option value="header">헤더</option>
				<option value="area_code">지역</option>
				<option value="writer">작성자</option>
			</select>
			<input type="text" id="keyword" name="keyword" value="<%if(request.getParameter("keyword") != null){ out.print(request.getParameter("keyword")); } else { out.print(""); }%>" />
			<select id="type1" name="type1">
				<option value="subject">제목</option>
				<option value="content">내용</option>
				<option value="header">헤더</option>
				<option value="area_code">지역</option>
				<option value="writer">작성자</option>
			</select>
			<input type="text" id="keyword1" name="keyword1" value="<%if(request.getParameter("keyword1") != null){ out.print(request.getParameter("keyword1")); } else { out.print(""); }%>" />
			<input type="submit" value="검색" />			
		</form>
	</div>	
	
	<table border="0" class="boardTable">
	   <colgroup>
	       <col width="5%"/>
	       <col width="5%"/>
	       <col width="5%"/>
	       <col width="45%"/>
	       <col width="10%"/>
	       <col width="5%"/>
	       <col width="5%"/>
	       <col width="5%"/>
	       <col width="15%"/>
	   </colgroup>
		<thead>
		<tr>
			<th>글번호</th>
			<th>헤더</th>
			<th>지역</th>
			<th>제목</th>
			<th>작성자</th>
			<th>댓글수</th>
			<th>조회수</th>
			<th>추천수</th>
			<th>작성일</th>	
		</tr>
		</thead>
		<tbody>
		<c:forEach var="board" items="${boardList}">
		<tr onclick="view(${board.idx})" onmouseover="this.className='mouseover'" onmouseout="this.className=''">
			<td class="idx">${board.idx}</td>
			<td>
	            <c:forEach var="headerItem" items="${headerList}">
	                <c:if test="${headerItem.code_value == board.header}">${headerItem.code_desc}</c:if>
	            </c:forEach>
            </td>
			<td>
                <c:forEach var="areaItem" items="${areaList}">
                    <c:if test="${areaItem.code_value == board.areaCode}">${areaItem.code_desc}</c:if>
                </c:forEach>
            </td>
			<td align="left" class="subject">
				<c:if test="${board.comment >= 10}"><img src="<%=request.getContextPath()%>/img/hit.jpg" /></c:if>
				${board.subject}</td>
			<td><c:choose><c:when test="${board.writerId == userId}"><strong>${board.writer}</strong></c:when><c:otherwise>${board.writer}</c:otherwise></c:choose></td>
			<td>${board.comment}</td>
			<td>${board.hitcount}</td>
			<td>${board.recommendcount}</td>
			<td>${board.writeDate}</td>		
		</tr>
		</c:forEach>
		</tbody>
	</table>
	<br />
	${pageHtml}
	<br /><br />
	<input type="button" value="목록" class="writeBt" onclick="moveAction(2)"/>
	<input type="button" value="쓰기" class="writeBt" onclick="moveAction(1)"/>	
</div>
</body>
</html>