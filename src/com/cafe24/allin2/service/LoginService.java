package com.cafe24.allin2.service;

import org.springframework.orm.ibatis.SqlMapClientTemplate;

import com.cafe24.allin2.dao.LoginDao;
import com.cafe24.allin2.model.LoginSessionModel;


public class LoginService implements LoginDao {
	private SqlMapClientTemplate sqlMapClientTemplate;
	
	public void setSqlMapClientTemplate(SqlMapClientTemplate sqlMapClientTemplate) {
		this.sqlMapClientTemplate = sqlMapClientTemplate;
	}

	@Override
	public LoginSessionModel checkUserId(String userId) {
		return (LoginSessionModel) sqlMapClientTemplate.queryForObject("login.loginCheck", userId);

	}	
}
