package com.cafe24.allin2.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.cafe24.allin2.dao.ShopDao;
import com.cafe24.allin2.model.DriverModel;
import com.cafe24.allin2.model.ShopModel;

public class ShopService extends SqlMapClientDaoSupport implements ShopDao {
	private HashMap<String, Object> valueMap = new HashMap<String, Object>();
		

	@Override
	public List<ShopModel> getShopList(int startArticleNum, int endArticleNum) {
		valueMap.put("startArticleNum", startArticleNum);
		valueMap.put("endArticleNum", endArticleNum);
		return getSqlMapClientTemplate().queryForList("shop.getShopList", valueMap);
	}
	
	@Override
	public ShopModel getOne(String shopCode) {
		return (ShopModel) getSqlMapClientTemplate().queryForObject("shop.getOne", shopCode);
	}

	@Override
	public List<ShopModel> searchForPage(Map<String,String> searchMap, int startArticleNum, int endArticleNum) {
		valueMap.putAll(searchMap);
		valueMap.put("startArticleNum", startArticleNum);
		valueMap.put("endArticleNum", endArticleNum);
		return getSqlMapClientTemplate().queryForList("shop.searchForPage", valueMap);
	}
	

	public List<ShopModel> search(Map<String, String> searchMap) {
		valueMap.putAll(searchMap);
		return getSqlMapClientTemplate().queryForList("shop.search", valueMap);
	}	
	
	@Override
	public boolean write(ShopModel shop) {
		
		getSqlMapClientTemplate().insert("shop.write", shop);
		return true;
	}

	@Override
	public void updateHitcount(int hitcount, int idx) {
		valueMap.put("hitcount", hitcount);
		valueMap.put("idx", idx);
		getSqlMapClientTemplate().update("shop.updateHitcount", valueMap);		
	}

	@Override
	public void updateRecommendCount(int recommendcount, int idx) {
		valueMap.put("recommendcount", recommendcount);
		valueMap.put("idx", idx);
		getSqlMapClientTemplate().update("shop.updateRecommendcount", valueMap);
		
	}
	@Override
	public int getTotalNum() {
		return (Integer) getSqlMapClientTemplate().queryForObject("shop.getTotalNum");
	}

	@Override
	public int getSearchTotalNum(Map<String,String> searchMap) {
		valueMap.putAll(searchMap);
		return (Integer) getSqlMapClientTemplate().queryForObject("shop.getSearchTotalNum", valueMap);
	}

	@Override
	public boolean delete(String shopCode) {
		getSqlMapClientTemplate().delete("shop.delete", shopCode);
		return true;
	}

	@Override
	public boolean modify(ShopModel shop) {
		getSqlMapClientTemplate().update("shop.modify", shop);
		return true;
	}

	@Override
	public boolean updateBannerCount(String shopCode) {
		getSqlMapClientTemplate().update("shop.updateBannerCount", shopCode);
		return true;
	}

	@Override
	public boolean updateSearchCount(String shopCode) {
		getSqlMapClientTemplate().update("shop.updateSearchCount", shopCode);
		return true;
		
	}

	@Override
	public List<DriverModel> searchDriver(Map<String, String> searchMap) {
		valueMap.putAll(searchMap);
		return getSqlMapClientTemplate().queryForList("shop.searchDriver", valueMap);
	}

	@Override
	public String getNewShopCode() {
		
		return (String)getSqlMapClientTemplate().queryForObject("shop.getNewShopCode");
		
	}


}
