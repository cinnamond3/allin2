package com.cafe24.allin2.services;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

import com.cafe24.allin2.model.BannerModel;
import com.cafe24.allin2.model.ShopModel;
import com.cafe24.allin2.service.BannerService;
import com.cafe24.allin2.service.BoardService;
import com.cafe24.allin2.service.ShopService;
import com.ibatis.common.logging.Log;

public class BannerServiceTest {

	@Test
	public void testGetList() {
		
		ApplicationContext context = new GenericXmlApplicationContext("/config/applicationContext.xml");
		BannerService bannerService = (BannerService) context.getBean("bannerService");
		List<BannerModel> banners = bannerService.getList();
		
		System.out.println("banners > " + banners);
	}
	
	@Test
	public void testSearch() {
		
		ApplicationContext context = new GenericXmlApplicationContext("/config/applicationContext.xml");
		BannerService bannerService = (BannerService) context.getBean("bannerService");
		Map<String,String> searchMap = new HashMap<String,String>();
		searchMap.put("type", "area_code");
		searchMap.put("keyword", "AA");
		searchMap.put("type1", "category");
		searchMap.put("keyword1", "BB");
		List<BannerModel> banners = bannerService.search(searchMap);
		
		System.out.println("banners > " + banners);
	}
	
	@Test
	public void testSearch2() {
		
		ApplicationContext context = new GenericXmlApplicationContext("/config/applicationContext.xml");
		BannerService bannerService = (BannerService) context.getBean("bannerService");
		
		Map<String,String> searchMap = new HashMap<String,String>();
		searchMap.put("type", "area_code");
		searchMap.put("keyword", "AA");
		searchMap.put("type1", "");
		searchMap.put("keyword1", "");
		
		List<BannerModel> banners = bannerService.search(searchMap);
		
		System.out.println("banners > " + banners);
	}
	
	@Test
	public void testGetOne() {
		
		ApplicationContext context = new GenericXmlApplicationContext("/config/applicationContext.xml");
		BannerService bannerService = (BannerService) context.getBean("bannerService");
		BannerModel banner = bannerService.getOne("1");
		
		System.out.println("banners > " + banner);
	}
	
	@Test
	public void testModify() {
		
		ApplicationContext context = new GenericXmlApplicationContext("/config/applicationContext.xml");
		BannerService bannerService = (BannerService) context.getBean("bannerService");
		
		BannerModel banner = new BannerModel();
		banner.setBannerId("3");
		banner.setAreaCode("AA");
		banner.setCategory("BB");
		banner.setImgFileName("3.jpg");
		banner.setShopCode("10002");
		banner.setWebUrl("http://test.com");
		
		
		bannerService.modify(banner);
		System.out.println("banner > " + banner);
	}
	

}
