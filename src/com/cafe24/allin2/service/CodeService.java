package com.cafe24.allin2.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.SqlMapClientTemplate;

  



import com.cafe24.allin2.dao.CodeDao;
import com.cafe24.allin2.model.CodeModel;

public class CodeService implements CodeDao {
	private SqlMapClientTemplate sqlMapClientTemplate;
	private HashMap<String, Object> valueMap = new HashMap<String, Object>();
		
 
	public void setSqlMapClientTemplate(SqlMapClientTemplate sqlMapClientTemplate) {
		this.sqlMapClientTemplate = sqlMapClientTemplate;
	}
  
	@Override
	public List<CodeModel> getCodeList(int startCodeNum, int endCodeNum) {
		valueMap.put("startCodeNum", startCodeNum);
		valueMap.put("endCodeNum", endCodeNum);
		return sqlMapClientTemplate.queryForList("code.getCodeList", valueMap);
	}
	
	@Override
	public List<CodeModel> getCodeList(String codeName) {
		return sqlMapClientTemplate.queryForList("code.getCodeList2", codeName);
	}

	@Override
	public CodeModel getOneCode(Map<String,String> searchMap) {
		valueMap.putAll(searchMap);
		return (CodeModel) sqlMapClientTemplate.queryForObject("code.getOneCode", valueMap);
	}

	  
	@Override 
	public List<CodeModel> searchCode(Map<String,String> searchMap, int startCodeNum, int endCodeNum) {
		valueMap.putAll(searchMap);
		valueMap.put("startCodeNum", startCodeNum);
		valueMap.put("endCodeNum", endCodeNum);
		return sqlMapClientTemplate.queryForList("code.searchCode", valueMap);
	}
	 
	
	@Override 
	public List<CodeModel> moblieSearchCode(Map<String,String> searchMap) {
		valueMap.putAll(searchMap);
		 
		return sqlMapClientTemplate.queryForList("code.mobileSearchCode", valueMap);
	}


	 

	@Override
	public boolean writeCode(CodeModel code) {
		sqlMapClientTemplate.insert("code.writeCode", code);		
		return true;
	}

 
 
	@Override
	public int getTotalNum() {
		return (Integer) sqlMapClientTemplate.queryForObject("code.getTotalNum");
	}

	@Override
	public int getSearchTotalNum(Map<String,String> searchMap) {
		valueMap.putAll(searchMap);
		return (Integer) sqlMapClientTemplate.queryForObject("code.getSearchTotalNum", valueMap);
	}
  
	
	@Override 
	public void deleteCode(CodeModel code) {
		sqlMapClientTemplate.delete("code.deleteCode", code);
	}

 
	@Override
	public boolean modifyCode(CodeModel code) {
		sqlMapClientTemplate.update("code.modifyCode", code);
		return true;
	}	

}
