<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>게시판 글 목록 보기</title>
<link href="<%=request.getContextPath()%>/css/shop.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript">
<!--
	function selectedOptionCheck(){
		$("#type > option[value=<%=request.getParameter("type")%>]").attr("selected", "true");
		$("#type1 > option[value=<%=request.getParameter("type1")%>]").attr("selected", "true");
	}
	
	function view(index){
	    location.href = "view.do?shopCode="+index;
	}

	function moveAction(where){
		switch (where) {
		case 1:
			location.href = "write.do";
			break;
		
		case 2:
			location.href = "list.do";
			break;
		}
	}
//-->
</script>
</head>
<body onload="selectedOptionCheck()">
<div class="wrapper">
	<div class="nav">
		<jsp:include page="/common/menu.jsp"></jsp:include>
	</div>
	<div class="header">
		<form action="list.do" method="get">		
			<select id="type" name="type">
				<option value="area_code">지역코드</option>
				<option value="shop_code">업체코드</option>
				<option value="shop_name">업체명</option>
				<option value="phone">전화번호</option>
			</select>
			<input type="text" id="keyword" name="keyword" value="<%if(request.getParameter("keyword") != null){ out.print(request.getParameter("keyword")); } else { out.print(""); }%>" />
			<select id="type1" name="type1">
				<option value="category">카테고리</option>
				<option value="shop_code">업체코드</option>
				<option value="shop_name">업체명</option>
				<option value="phone">전화번호</option>
			</select>
			<input type="text" id="keyword1" name="keyword1" value="<%if(request.getParameter("keyword1") != null){ out.print(request.getParameter("keyword1")); } else { out.print(""); }%>" />
			<input type="submit" value="검색" />			
		</form>
	</div>	
	
	<table border="0" class="boardTable">
		<colgroup>
		<col width="4%"/>
		<col width="4%"/>
		<col width="4%"/>
		<col width="15%"/>
		<col width="9%"/>
		<col width="9%"/>
		<col width="15%"/>
		<col width="14%"/>
		<col width="9%"/>
		<col width="9%"/>
		<col width="4%"/>
		<col width="4%"/>
		</colgroup>
		<thead>
		<tr>
			<th>업체코드</th>
			<th>지역</th>
			<th>카테고리</th>
			<th>업체명</th>
			<th>전화번호</th>
			<th>간략주소</th>
			<th>배달지역</th>
			<th>영업시간</th>
			<th>휴무일</th>
			<th>사용유무</th>
			<th>업체 등급</th>

		</tr>
		</thead>
		<tbody>
		<c:forEach var="shop" items="${shopList}">
		<tr onclick="view(${shop.shopCode})" onmouseover="this.className='mouseover'" onmouseout="this.className=''">
			<td class="">${shop.shopCode}</td>
            <td>
                <c:forEach var="areaItem" items="${areaList}">
                    <c:if test="${areaItem.code_value == shop.areaCode}">${areaItem.code_desc}</c:if>
                </c:forEach>
            </td>
            <td>
                <c:forEach var="categoryItem" items="${categoryList}">
                    <c:if test="${categoryItem.code_value == shop.category}">${categoryItem.code_desc}</c:if>
                </c:forEach>
            </td>
			<td align="left">${shop.shopName}</td>
			<td class="">${shop.phone}</td>
			<td class="">${shop.addressTail}</td>
			<td class="">${shop.deliverlyArea}</td>		
			<td class="">${shop.shopHours}</td>		
			<td class="">${shop.offDays}</td>		
			<td class="">${shop.useYn}</td>
            <td>
                <c:forEach var="shopGradeItem" items="${shopGradeList}">
                    <c:if test="${shopGradeItem.code_value == shop.shopGrade}">${shopGradeItem.code_desc}</c:if>
                </c:forEach>
            </td>
		</tr>
		</c:forEach>
		</tbody>
	</table>
	<br />
	${pageHtml}
	<br /><br />
	<input type="button" value="목록" class="writeBt" onclick="moveAction(2)"/>
	<input type="button" value="쓰기" class="writeBt" onclick="moveAction(1)"/>	
</div>
</body>
</html>