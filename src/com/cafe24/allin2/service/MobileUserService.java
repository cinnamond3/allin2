package com.cafe24.allin2.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.SqlMapClientTemplate;

 





import com.cafe24.allin2.dao.MobileUserDao;
import com.cafe24.allin2.model.CodeModel;
import com.cafe24.allin2.model.MobileUserModel;

public class MobileUserService implements MobileUserDao {
	private SqlMapClientTemplate sqlMapClientTemplate;
	private HashMap<String, Object> valueMap = new HashMap<String, Object>();
		

	public void setSqlMapClientTemplate(SqlMapClientTemplate sqlMapClientTemplate) {
		this.sqlMapClientTemplate = sqlMapClientTemplate;
	}
  
	@Override
	public List<MobileUserModel> getMobileUserList(int startMobileUserNum, int endMobileUserNum) {
		valueMap.put("startMobileUserNum", startMobileUserNum);
		valueMap.put("endMobileUserNum", endMobileUserNum);
		return sqlMapClientTemplate.queryForList("mobileuser.getMobileUserList", valueMap);
		
	}
 
	@Override
	public List<MobileUserModel> searchMobileUser(Map<String,String> searchMap,  int startMobileUserNum, int endMobileUserNum) {
		valueMap.putAll(searchMap);
		  
		valueMap.put("startMobileUserNum", startMobileUserNum);
		valueMap.put("endMobileUserNum", endMobileUserNum);
		return sqlMapClientTemplate.queryForList("mobileuser.searchMobileUser", valueMap);
	}
	
	@Override
	public  boolean moblieRegistMobileUser (MobileUserModel mobileUser ) {
	  	sqlMapClientTemplate.insert("mobileuser.registMobileUser", mobileUser);
		return true;
	}
	
	 
	@Override
	public int getTotalNum() {
		return (Integer) sqlMapClientTemplate.queryForObject("mobileuser.getTotalNum");
	}

	@Override
	public int getSearchTotalNum(Map<String,String> searchMap) {
		valueMap.putAll(searchMap);
		 
		return (Integer) sqlMapClientTemplate.queryForObject("mobileuser.getSearchTotalNum", valueMap);
	}
	
	//존재 여부 확인
	@Override
	public MobileUserModel getMobileUserOne(String phone_number) {
		valueMap.put("phone_number", phone_number);
		return (MobileUserModel) sqlMapClientTemplate.queryForObject("mobileuser.getMobileUserOne", valueMap);
	}
	
	//존재시 변경 regId
  	@Override
	public void updateMobileUser(String phone_number, String area_code, String reg_id, String use_yn ) {
		valueMap.put("phone_number", phone_number);
		valueMap.put("area_code", area_code);
		valueMap.put("reg_id", reg_id);
		valueMap.put("use_yn", use_yn); 
		 sqlMapClientTemplate.insert("mobileuser.updateMobileUser", valueMap);		
		   
	}
  	@Override 
	public List<CodeModel> searchAreaCode(Map<String,String> searchMap) {
		valueMap.putAll(searchMap);
		 
		return sqlMapClientTemplate.queryForList("mobileuser.searchAreaCode", valueMap);
	}
}
