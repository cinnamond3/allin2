package com.cafe24.allin2.model;

import java.util.ArrayList;
import java.util.List;

public class SimpleShopModelConveter {
	
	public List<SimpleShopModel> covertList(List<ShopModel> shopList) {
		List<SimpleShopModel> simpleShopList = new ArrayList<SimpleShopModel>();
		for (int inx=0; inx<shopList.size(); inx++) {
			simpleShopList.add(new SimpleShopModel(shopList.get(inx)));
		}
		return simpleShopList;
	}

}
