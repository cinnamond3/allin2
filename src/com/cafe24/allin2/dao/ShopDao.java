package com.cafe24.allin2.dao;

import java.util.List;
import java.util.Map;

import com.cafe24.allin2.model.DriverModel;
import com.cafe24.allin2.model.ShopModel;

public interface ShopDao {
	// get all contents in JMShop table
	List<ShopModel> getShopList(int startArticleNum, int showArticleLimit);
	
	// show detail about selected article
	ShopModel getOne(String shopCode);
	
	// get search result list
	List<ShopModel> searchForPage(Map<String,String> searchMap, int startArticleNum, int endArticleNum);
	
	// modify(update) article
	boolean modify(ShopModel shop);
	
	// insert article data
	boolean write(ShopModel shop);
	
	// update hitcount
	void updateHitcount(int hitcount, int idx);
	
	// update recommendcount
	void updateRecommendCount(int recommendcount, int idx);
	
	// get contents count
	int getTotalNum();
	
	// get count of search results
	int getSearchTotalNum(Map<String,String> searchMap);
	
	boolean delete(String shopCode);

	boolean updateBannerCount(String shopCode);

	boolean updateSearchCount(String shopCode);

	List<DriverModel> searchDriver(Map<String, String> searchMap);

	String getNewShopCode();

}
