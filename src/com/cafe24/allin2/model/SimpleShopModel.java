package com.cafe24.allin2.model;

public class SimpleShopModel {
	   
	private String shopCode;
	private String shopName;
	private String phone;
	private String addressTail;
	private String creditYn;
	private String deliverlyYn;
	private String menuYn;
	private String couponYn;
	private String deliverlyArea;
	private String shopHours;
	private String smallLogImgFileName;
	
	
	public SimpleShopModel() {
	}
	
	public SimpleShopModel(ShopModel shopModel) {
		this.shopCode = shopModel.getShopCode();
		this.shopName = shopModel.getShopName();
		this.phone = shopModel.getPhone();
		this.addressTail = shopModel.getAddressTail();
		this.creditYn = shopModel.getCreditYn();
		this.deliverlyYn = shopModel.getDeliverlyYn();
		this.menuYn = shopModel.getMenuYn();
		this.couponYn = shopModel.getCouponYn();
		this.deliverlyArea = shopModel.getDeliverlyArea();
		this.shopHours = shopModel.getShopHours();
		this.smallLogImgFileName = shopModel.getSmallLogImgFileName();
	}
	
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddressTail() {
		return addressTail;
	}
	public void setAddressTail(String addressTail) {
		this.addressTail = addressTail;
	}
	public String getCreditYn() {
		return creditYn;
	}
	public void setCreditYn(String creditYn) {
		this.creditYn = creditYn;
	}
	public String getDeliverlyYn() {
		return deliverlyYn;
	}
	public void setDeliverlyYn(String deliverlyYn) {
		this.deliverlyYn = deliverlyYn;
	}
	public String getMenuYn() {
		return menuYn;
	}
	public void setMenuYn(String menuYn) {
		this.menuYn = menuYn;
	}
	public String getCouponYn() {
		return couponYn;
	}
	public void setCouponYn(String couponYn) {
		this.couponYn = couponYn;
	}
	public String getDeliverlyArea() {
		return deliverlyArea;
	}
	public void setDeliverlyArea(String deliverlyArea) {
		this.deliverlyArea = deliverlyArea;
	}
	public String getShopHours() {
		return shopHours;
	}
	public void setShopHours(String shopHours) {
		this.shopHours = shopHours;
	}
	public String getSmallLogImgFileName() {
		return smallLogImgFileName;
	}
	public void setSmallLogImgFileName(String smallLogImgFileName) {
		this.smallLogImgFileName = smallLogImgFileName;
	}

	public String toString() {
		StringBuffer sbString = new StringBuffer();
		sbString.append("{shopCode=").append(shopCode).append(", ");
		sbString.append("shopName=").append(shopName).append(", ");
		sbString.append("phone=").append(phone).append(", ");
		sbString.append("addressTail=").append(addressTail).append(", ");
		sbString.append("creditYn=").append(creditYn).append(", ");
		sbString.append("deliverlyYn=").append(deliverlyYn).append(", ");
		sbString.append("menuYn=").append(menuYn).append(", ");
		sbString.append("couponYn=").append(couponYn).append(", ");
		sbString.append("deliverlyArea=").append(deliverlyArea).append(", ");
		sbString.append("shopHours=").append(shopHours).append(", ");
		sbString.append("smallLogImgFileName=").append(smallLogImgFileName).append(", ");
		return sbString.toString();
	}
}
