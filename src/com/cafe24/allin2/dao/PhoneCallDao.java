package com.cafe24.allin2.dao;

import java.util.List;








import java.util.Map;

import com.cafe24.allin2.model.PhoneCallModel;

public interface PhoneCallDao {
	// get all contents in JMPhoneCall table
	List<PhoneCallModel> getPhoneCallList(int startPhoneCallNum, int showPhoneCallLimit);
	 
	// get search result list
	List<PhoneCallModel> searchPhoneCall(Map<String,String> searchMap,  String startDate, String EndDate, int startPhoneCallNum, int endPhoneCallNum); 
    
	boolean moblieRegistPhoneCall(PhoneCallModel phoneCall );
	    
	// get contents count
	int getTotalNum();
	
	// get count of search results
	int getSearchTotalNum(Map<String,String> searchMap, String startDate, String endDate);
	
	// update phone call count 
	void updateShopCallCount(String shop_code );
	
	boolean insertShopCallCount(String shop_code );
	
	int getShopCallNum(String shop_code);
	
	boolean mobileGPSRegist (String provider,String latitude,String longitude );
}
