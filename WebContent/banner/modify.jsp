<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>배너 수정: ${shop.shopName}</title>
<link href="<%=request.getContextPath()%>/css/banner.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript">
	function writeFormCheck() {
		/*
		if($("#areaCode").val() == null || $("#areaCode").val() == ""){
			alert("지역을 입력해 주세요!");
			$("#areaCode").focus();
			return false;
		}
		*/
		
		return true;
	}
	
	function moveAction(where){
		switch (where) {
		case 1:
			location.href ="list.do";
			break;
		}
	}
	
	function changeEvent(selectObj){
        document.getElementById("shopCode").value = selectObj.value;
    }
</script>
</head>
<body>
<div class="wrapper">
    <div class="nav">
        <jsp:include page="/common/menu.jsp"></jsp:include>
    </div>  	
	<h3>업체 수정</h3>
	<form action="modify.do" method="post" onsubmit="return writeFormCheck()" enctype="multipart/form-data">
	    <input type="hidden" id="writer" name="writer" value="${userName}" />
	    <input type="hidden" id="writerId" name="writerId" value="${userId}" />
	
		<table class="write">
	       <colgroup>
	           <col width="20%">
	           <col width="80%">
	       </colgroup>
	        <tr>
			    <th>배너ID</th>
			    <td>${banner.bannerId}<input type="hidden" id="bannerId" name="bannerId" value="${banner.bannerId}" /></td>
			</tr>
			<tr>
                <th><label for="areaCode">지역</label></th>
	            <td>
	                <c:forEach var="areaItem" items="${areaList}">
	                    <c:if test="${areaItem.code_value == banner.areaCode}">${areaItem.code_desc}</c:if>
	                </c:forEach>
	            </td>
            </tr>
            <tr>
                <th><label for="categoty">카테고리</label></th>
                <td>
                    <select id="category" name=category>
                        <c:forEach var="categoryItem" items="${categoryList}">
                            <option value="<c:out value="${categoryItem.code_value }" />" 
                                <c:if test="${categoryItem.code_value == banner.category}">selected="selected"</c:if>>
                                <c:out value="${categoryItem.code_desc }" />
                            </option>
                        </c:forEach>
                    </select>
                </td>
			</tr>
			<tr>
			    <th>배너 Image</th>
                <td>
                    <input type="file" id="newImgFile" name="newImgFile" />
                    <br/>
                    <c:if test="${banner.imgFileName != null }">
                        <span class="gray">
                            등록된 파일 : ${banner.imgFileName}
                            <br/>
                            <input type="hidden" id="imgFileName" name="imgFileName" value="${banner.imgFileName}" />
                            <input type="checkbox" id="imgFileDeleteFlag" name="imgFileDeleteFlag" />파일삭제시 체크
                            <br/>
                            <img class="imgFile" src="<%=request.getContextPath()%>/banner/upload/${banner.imgFileName}"/>
                        </span>
                    </c:if>
                </td>
			</tr>
	        <tr>
	            <th rowspan="2">업체링크</th>
	            <td>
	            <select id="selectShopCode" name=selectShopCode onchange="changeEvent(this)">
	            <c:forEach var="shopItem" items="${shopList}">
	                <option value="<c:out value="${shopItem.shopCode }"/>" 
	                    <c:if test="${shopItem.shopCode == banner.shopCode}">selected="selected"</c:if>>
	                    <c:out value="[${shopItem.shopCode}][${shopItem.shopName}] ${shopItem.addressHeader} ${shopItem.phone}" />
	                </option>
	            </c:forEach>
	            </select>
	            </td>
	        </tr>
	        <tr>
	            
			    <td>
				    <input type="text" id="shopCode" name="shopCode" value="${banner.shopCode}" /> 
		            <br/>
		            <span>
		            * 같은 지역에 해당되는 업체가 선택박스에서 조회됩니다.<br/>
		            * 직접 URL을 입력하시면 그 링크로 바로 연결됩니다.
		            </span>
	            </td>
	        </tr>
			<tr>
			    <th>웹 URL링크</th>
			    <td><input type="text" id="webUrl" name="webUrl" value="${banner.webUrl}" /></td>
			</tr>
	    </table>
    	
		<input type="reset" value="재작성" class="writeBt"/>
		<input type="submit" value="확인" class="writeBt"/>	
		<input type="button" value="목록" class="writeBt" onclick="moveAction(1)" />
	</form>
</div>
</body>
</html>