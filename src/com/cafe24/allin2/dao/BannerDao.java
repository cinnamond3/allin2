package com.cafe24.allin2.dao;

import java.util.List;

import com.cafe24.allin2.model.BannerModel;

public interface BannerDao {

	List<BannerModel> getList();

	BannerModel getOne(String bannerId);

	boolean modify(BannerModel bannerModel);

	String getNewBannerId();

	boolean write(BannerModel banner);

	boolean delete(String bannerId);

}
