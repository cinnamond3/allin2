package com.cafe24.allin2.dao;

import com.cafe24.allin2.model.LoginSessionModel;




public interface LoginDao {
	
	LoginSessionModel checkUserId(String userId);

}
