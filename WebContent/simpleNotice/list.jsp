
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>글 보기: ${board.subject}</title>
<link href="<%=request.getContextPath()%>/css/simpleNotice.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    
    function moveAction(where){
        switch (where) {
        case 2:
            if(confirm("글을 수정하시겠습니까?")){
                location.href = "modify.do";
            }
            break;
        }
    }
    
    function view(index){
        location.href = "view.do?areaCode="+index;
    }
</script>
</head>
<body onload="">
<div class="wrapper">   
    <div class="nav">
        <jsp:include page="/common/menu.jsp"></jsp:include>
    </div>  
    <table class="view">
        <colgroup>
          <col width="10%">
          <col width="30%">
          <col width="30%">
          <col width="30%">
        </colgroup>
        <tr>
            <th>지역</th>
            <th>APP 메인 한줄공지</th>
            <th>APP 메인 한줄공지링크</th>
            <th>APP 메인 팝업공지</th>
        </tr>
        
        <c:forEach var="simpleNotice" items="${simpleNoticeList}">
        <tr onclick="view('${simpleNotice.areaCode}')" onmouseover="this.className='mouseover'" onmouseout="this.className=''">
            <td>
                <c:forEach var="areaItem" items="${areaList}">
                    <c:if test="${areaItem.code_value == simpleNotice.areaCode}">${areaItem.code_desc}</c:if>
                </c:forEach>
            </td>
            <td>${simpleNotice.notifyText}</td>
            <td>${simpleNotice.notifyLink}</td>
            <td>${simpleNotice.popupText}</td>
        </tr>
        </c:forEach>
        
    </table>    
    <br/>
</div>
</body>
</html>