package com.cafe24.allin2.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.cafe24.allin2.helpservice.WebHelpService;
import com.cafe24.allin2.model.BoardModel;
import com.cafe24.allin2.model.CodeModel;
import com.cafe24.allin2.model.SimpleNoticeModel;
import com.cafe24.allin2.service.BoardService;
import com.cafe24.allin2.service.CachedCodeService;
import com.cafe24.allin2.service.CodeService;
import com.cafe24.allin2.service.SimpleNoticeService;

@Controller
@RequestMapping("/simpleNotice")
public class SimpleNoticeController {
	// DI
	private ApplicationContext context = new GenericXmlApplicationContext("/config/applicationContext.xml");
	private SimpleNoticeService service = (SimpleNoticeService) context.getBean("simpleNoticeService");
	private BoardService boardService = (BoardService) context.getBean("boardService");
	
	private CachedCodeService cachedCodeService = new CachedCodeService();
	private WebHelpService webHelpService = new WebHelpService();
	
	@RequestMapping("/list.do")
	public ModelAndView list(HttpServletRequest request, HttpServletResponse response){
		
		List<CodeModel> areaList = cachedCodeService.getCodeList("AREA");
		List<CodeModel> headerList = cachedCodeService.getCodeList("HEADER");
		
		List<SimpleNoticeModel> simpleNoticeList = service.getList();
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("simpleNoticeList", simpleNoticeList);
		mav.addObject("headerList", headerList);
		mav.addObject("areaList", areaList);
		mav.setViewName("/simpleNotice/list");
		
		return mav;
	}
	
	@RequestMapping("/view.do")
	public ModelAndView simpleNoticeView(HttpServletRequest request){
		
		List<CodeModel> areaList = cachedCodeService.getCodeList("AREA");
		String areaCode = request.getParameter("areaCode");
		
		ModelAndView mav = new ModelAndView();
			
		SimpleNoticeModel simpleNotice = service.getOne(areaCode);
		mav.addObject("simpleNotice", simpleNotice);
		mav.addObject("areaList", areaList);
			 
		mav.setViewName("/simpleNotice/view");
		
		return mav;
		 
	}
	
	@RequestMapping("/mobile/view.do")
	@ResponseBody
	public SimpleNoticeModel simpleNoticeJsonView(HttpServletRequest request){
		String areaCode = request.getParameter("areaCode");
		SimpleNoticeModel simpleNotice = service.getOne(areaCode);
		return simpleNotice;
		
	}

/*	 
	
	@RequestMapping("/write.do")
	public String CodeWrite(@ModelAttribute("CodeModel") CodeModel CodeModel){		
		return "/code/write";
	}
	
	@RequestMapping(value="/write.do", method=RequestMethod.POST)
	public String CodeWriteProc(@ModelAttribute("CodeModel") CodeModel CodeModel, MultipartHttpServletRequest request){
		// get upload file
	 
		//
		// new line code change to <br /> tag	
		 
		//
		codeService.writeCode(CodeModel);		
		
		return "redirect:list.do";
	}
	 
	
	 
	
 */
	
	@RequestMapping("/modify.do")
	public ModelAndView boardModify(HttpServletRequest request, HttpSession session){
		
		List<CodeModel> areaList = cachedCodeService.getCodeList("AREA");
		String areaCode = request.getParameter("areaCode");
		SimpleNoticeModel simpleNotice = service.getOne(areaCode);
		Map<String, String> searchMap = new HashMap<String,String>();
		
		List<BoardModel> boardList = boardService.search(searchMap, 1, 100, BoardModel.DATE_FORMAT2, simpleNotice.getAreaCode(), "");
		
		ModelAndView mav = new ModelAndView();

		mav.addObject("simpleNotice", simpleNotice);
		mav.addObject("areaList", areaList);
		mav.addObject("boardList", boardList);
		mav.setViewName("/simpleNotice/modify");
		return mav;
	}
	
	@RequestMapping(value = "/modify.do", method=RequestMethod.POST)
	public ModelAndView CodeModifyProc(@ModelAttribute("CodeModel") SimpleNoticeModel simpleNotice, MultipartHttpServletRequest request){
		 
		
		service.modify(simpleNotice);
		
		ModelAndView mav = new ModelAndView();
		 
		mav.setViewName("redirect:/simpleNotice/list.do");
		return mav;
	}  
}
