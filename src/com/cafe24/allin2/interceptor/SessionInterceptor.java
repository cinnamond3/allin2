package com.cafe24.allin2.interceptor;

import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


public class SessionInterceptor extends HandlerInterceptorAdapter{
	
	Logger logger = Logger.getLogger("SessionInterceptor.class");
	
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		// check variable
		Object userId = request.getSession().getAttribute("userId");
		//
		
		// pass through when access login.do, join.do
		String requestURI = request.getRequestURI();
		
		logger.debug("requestURI > " + requestURI);
		
		if(requestURI.equals("/login.do")){
			if(userId != null){
				response.sendRedirect(request.getContextPath() + "/board/list.do");
				return true;
			} else {
				return true;
			}
		} else if (checkByPassURI(requestURI)){
			return true;
		}
		
		//
		// where other pages
		if(userId == null){
			
			response.sendRedirect(request.getContextPath() + "/login.do");
			return false;
		} else {
			return true;
		}
		//		
	}

	public boolean checkByPassURI(String requestURI) {
		String regex = "/\\w+/mobile/\\w+.do";
		Pattern byPassPattern = Pattern.compile(regex);
		return byPassPattern.matcher(requestURI).matches();
		//return true;
	}
	
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
	}
}
