package com.cafe24.allin2.dao;

import java.util.List;
import java.util.Map;

import com.cafe24.allin2.model.BoardCommentModel;
import com.cafe24.allin2.model.BoardModel;

public interface BoardDao {
	// get all contents in JMBoard table
	List<BoardModel> getList(int startArticleNum, int showArticleLimit, String dateFormat);
	
	// show detail about selected article
	BoardModel getOneArticle(int idx);
	
	// get search result list
	List<BoardModel> search(Map<String,String> searchMap, int startArticleNum, int endArticleNum, String dateFormat, String areaCode, String header);
	
	// get all comments
	List<BoardCommentModel> getCommentList(int idx);
	
	// get a comment
	BoardCommentModel getOneComment(int idx);
	
	// modify(update) article
	boolean modifyArticle(BoardModel board);
	
	// insert article data
	boolean writeArticle(BoardModel board);
	
	// insert comment data
	boolean writeComment(BoardCommentModel comment);
	
	// update hitcount
	void updateHitcount(int hitcount, int idx);
	
	// update recommendcount
	void updateRecommendCount(int recommendcount, int idx);
	
	// get contents count
	int getTotalNum();
	
	// get count of search results
	int getSearchTotalNum(Map<String,String> searchMap, String areaCode, String header);
	
	// delete a comment
	void deleteComment(int idx);
	
	// delete a article
	void deleteArticle(int idx);
}
