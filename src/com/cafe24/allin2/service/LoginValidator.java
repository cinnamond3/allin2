package com.cafe24.allin2.service;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.cafe24.allin2.model.LoginSessionModel;

public class LoginValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return LoginSessionModel.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		LoginSessionModel loginModel = (LoginSessionModel) target;
		
		// check userId field
		if(loginModel.getUserId() == null || loginModel.getUserId().trim().isEmpty()) {
			errors.rejectValue("userId", "required");
		}
		
		// check userPw field
		if(loginModel.getUserPw() == null || loginModel.getUserPw().trim().isEmpty()){
			errors.rejectValue("userPw", "required");
		}

	}

}
