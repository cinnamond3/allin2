package com.cafe24.allin2.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.io.IOException;

import javax.servlet.ServletException;

import org.junit.Test;

public class SimpleNoticeControllerTest extends AbstractDispatcherServletTest {

	
	@Test
	public void testSimpleNoticeJsonView() throws ServletException, IOException {
		setClasses(SimpleNoticeController.class);
		//initRequest("/simpleNotice/mobile/view.do").addParameter("name", "Spring");
		
		
		
		initRequest("/mobile/view.do");
		runService();
		assertThat(getContentAsString(), is("{\"messages\":\"Hello Spring\"}"));
	}

}
