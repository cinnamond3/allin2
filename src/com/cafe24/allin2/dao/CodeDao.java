package com.cafe24.allin2.dao;

import java.util.List;






import java.util.Map;

import com.cafe24.allin2.model.CodeModel;

public interface CodeDao {
	// get all contents in JMCode table
	List<CodeModel> getCodeList(int startCodeNum, int showCodeLimit);
	
	List<CodeModel> getCodeList(String codeName);
	
	// show detail about selected article
	CodeModel getOneCode(Map<String,String> searchMap);
	  
	// get search result list
	List<CodeModel> searchCode(Map<String,String> searchMap, int startCodeNum, int showCodeLimit); 
     
	List<CodeModel> moblieSearchCode(Map<String,String> searchMap);
	// modify(update) article
	boolean modifyCode(CodeModel Code);
	
	// insert article data
	boolean writeCode(CodeModel Code); 
	  
	// get contents count
	int getTotalNum();
	
	// get count of search results
	int getSearchTotalNum(Map<String,String> searchMap);
	 
	// delete a article
	void deleteCode(CodeModel Code);
}
