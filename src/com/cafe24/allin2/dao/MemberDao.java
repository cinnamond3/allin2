package com.cafe24.allin2.dao;

import com.cafe24.allin2.model.MemberModel;
;
public interface MemberDao {
	boolean addMember(MemberModel memberModel);
	MemberModel findByUserId(String userId);
}
