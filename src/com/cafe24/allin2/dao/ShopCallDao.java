package com.cafe24.allin2.dao;

import java.util.List;
import java.util.Map;

import com.cafe24.allin2.model.ShopCallModel;

public interface ShopCallDao {

	List<ShopCallModel> search(Map<String, String> searchMap,
			int startArticleNum, int endArticleNum);

	int getSearchTotalNum(Map<String, String> searchMap);

}
