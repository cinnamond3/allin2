<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>새 업체 등록</title>
<link href="<%=request.getContextPath()%>/css/shop.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript" src="/shop/js/shop.js"></script>
</head>
<body>
<div class="wrapper">
    <div class="nav">
        <jsp:include page="/common/menu.jsp"></jsp:include>
    </div>  	
	<h3>새 업체 등록</h3>

	<form action="write.do" method="post" onsubmit="return writeFormCheck()" enctype="multipart/form-data">
	    <input type="hidden" id="writer" name="writer" value="${userName}" />
	    <input type="hidden" id="writerid" name="writerid" value="${userId}" />
	
		<table class="write">
	       <colgroup>
	           <col width="15%">
	           <col width="35%">
	           <col width="15%">
	           <col width="35%">
	       </colgroup>
	        <tr>
	            <th><label for="areaCode">지역</label></th>
	            <td>
	                <select id="areaCode" name=areaCode>
	                    <c:forEach var="areaItem" items="${areaList}">
	                        <option value="<c:out value="${areaItem.code_value }" />">
	                            <c:out value="${areaItem.code_desc }" />
	                        </option>
	                    </c:forEach>
	                </select>
	            </td>
	            <th><label for="categoty">카테고리</label></th>
	            <td>
                    <select id="category" name=category>
                        <c:forEach var="categoryItem" items="${categoryList}">
                            <option value="<c:out value="${categoryItem.code_value }" />">
                                <c:out value="${categoryItem.code_desc }" />
                            </option>
                        </c:forEach>
                    </select>
                </td>
	        </tr>
	        <tr>
	            <th>업체명</th>
	            <td colspan="3"><input type="text" id="shopName" name="shopName" value="" /></td>
	        </tr>
	        <tr>
	            <th>전체주소</th>
	            <td colspan="3">
	               <input type="text" id="addressHeader" name="addressHeader" value="" />
	               <input type="button" id="getMapXY" value="좌표구하기" class="writeBt" />
	            </td>
	            
	        </tr>
	        <tr>
	            <th>간단주소</th>
	            <td colspan="3"><input type="text" id="addressTail" name="addressTail" value="" /></td>
	        </tr>
	        <tr>
	            <th>전화번호</th>
	            <td><input type="text" id="phone" name="phone" value="${shop.phone}" /></td>
	            <th>실제전화번호</th>
	            <td><input type="text" id="originalPhone" name="originalPhone" value="" /></td>
	        </tr>
	        <tr>
	            <th>배달지역</th>
	            <td><input type="text" id="deliverlyArea" name="deliverlyArea" value="" /></td>
	            <th>영업시간</th>
	            <td><input type="text" id="shopHours" name="shopHours" value="" /></td>
	        </tr>
	        <tr>
	            <th></th>
	            <td></td>
	            <th></th>
	            <td>* 영업시간 입력 예) 오후4시~새벽1시</td>
	        </tr>
	        <tr>
	            <th>카드여부</th>
	            <td>
	                <select id="creditYn" name=creditYn>
                        <option value="Y">Y</option>
                        <option value="N">N</option>
                    </select>
                </td>
	            <th>결재수단</th>
	            <td><input type="text" id="shopPay" name="shopPay" value="" /></td> 
	        </tr>
	        <tr>
	            <th>배달여부</th>
                <td>
                    <select id="deliverlyYn" name=deliverlyYn>
                        <option value="Y">Y</option>
                        <option value="N">N</option>
                    </select>
                </td>
	            <th>메뉴여부</th>
                <td>
                    <select id="menuYn" name=menuYn>
                        <option value="Y">Y</option>
                        <option value="N">N</option>
                    </select>
                </td>
	        </tr>
	        <tr>
	            <th>쿠폰여부</th>
                <td>
                    <select id="couponYn" name=couponYn>
                        <option value="Y">Y</option>
                        <option value="N">N</option>
                    </select>
                </td>
	            <th> </th>
	            <td> </td>
	        </tr>
	        <tr>
	            <th>업체설명</th>
	            <td><input type="text" id="shopDesc" name="shopDesc" value="" /></td> 
	            <th>휴무일</th>
	            <td><input type="text" id="offDays" name="offDays" value="" /></td>
	        </tr>
            <tr>
	            <th>메뉴 Image 파일명</th>
	            <td>
		            <input type="file" id="newMenuImgFile" name="newMenuImgFile" />
	            </td>
	            <th>Small Log Image 파일명</th>
	            <td>
	                <input type="file" id="newSmallLogImgFile" name="newSmallLogImgFile" />
	            </td>
	        </tr>
            <tr>
                <th>위도</th>
                <td class="">
                   <input type="text" id="latitude" name="latitude" value="${shop.latitude}"/>
                </td>
                   
                <th>경도</th>
                <td>
                   <input type="text" id="longitude" name="longitude" value="${shop.longitude}"/>
                </td>
            </tr>
	        <tr>
	            <th>사용유무</th>
	            <td>
                    <select id="useYn" name=useYn>
                        <option value="Y">Y</option>
                        <option value="N">N</option>
                    </select>
                </td>
	            <th>업체 등급</th>
	            <td>
                    <select id="shopGrade" name=shopGrade>
                        <c:forEach var="shopGradeItem" items="${shopGradeList}">
                            <option value="<c:out value="${shopGradeItem.code_value }" />">
                                <c:out value="${shopGradeItem.code_desc }" />
                            </option>
                        </c:forEach>
                    </select>
                </td>   
	        </tr>
	    </table>
    	
		<input type="reset" value="재작성" class="writeBt"/>
		<input type="submit" value="확인" class="writeBt"/>	
		<input type="button" value="목록" class="writeBt" onclick="moveAction(1)" />
	</form>
</div>
</body>
</html>