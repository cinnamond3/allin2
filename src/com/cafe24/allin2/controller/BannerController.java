package com.cafe24.allin2.controller;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.cafe24.allin2.helpservice.WebHelpService;
import com.cafe24.allin2.model.BannerModel;
import com.cafe24.allin2.model.CodeModel;
import com.cafe24.allin2.model.DriverModel;
import com.cafe24.allin2.model.ShopModel;
import com.cafe24.allin2.service.BannerService;
import com.cafe24.allin2.service.CachedCodeService;
import com.cafe24.allin2.service.ShopService;

@Controller
@RequestMapping("/banner")
public class BannerController {
	// DI
	private ApplicationContext context = new GenericXmlApplicationContext("/config/applicationContext.xml");
	private BannerService service = (BannerService) context.getBean("bannerService");
	private ShopService shopService = (ShopService) context.getBean("shopService");
	private WebHelpService webHelpService = new WebHelpService();
	private CachedCodeService cachedCodeService = new CachedCodeService();
	
	public static final String BANNER_UPLOAD_PATH = "banner/upload/";
	
	Logger logger = Logger.getLogger(BannerController.class);
	@RequestMapping("/list.do")
	public ModelAndView getList(HttpServletRequest request){
		
		Map<String,String> searchMap = webHelpService.getSearchMap(request);
		List<BannerModel> bannerList;
		if(webHelpService.isSearch(searchMap)){
			bannerList = service.search(searchMap);
		} else {
			bannerList = service.getList();
		}
		
		List<CodeModel> areaList = cachedCodeService.getCodeList("AREA");
		List<CodeModel> categoryList = cachedCodeService.getCodeList("CATEGORY");
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("bannerList", bannerList);
		mav.addObject("areaList", areaList);
		mav.addObject("categoryList", categoryList);
		mav.setViewName("/banner/list");
		return mav;
		
	}
	
	@RequestMapping("/mobile/list.do")
	@ResponseBody
	public List<BannerModel> getJsonList(HttpServletRequest request){
		
		String type = StringUtils.trimToEmpty(request.getParameter("type"));
		String keyword = StringUtils.trimToEmpty(request.getParameter("keyword"));
		String type1 = StringUtils.trimToEmpty(request.getParameter("type1"));
		String keyword1 = StringUtils.trimToEmpty(request.getParameter("keyword1"));

		
		Map<String,String> searchMap = new HashMap<String,String>();
		searchMap.put("type", type);
		searchMap.put("keyword", keyword);
		searchMap.put("type1", type1);
		searchMap.put("keyword1", keyword1);
		
		List<BannerModel> bannerList = null;
		
		if (type.equalsIgnoreCase("area_code") && !keyword.equals("") 
				&& type1.equalsIgnoreCase("category") && !keyword1.equals("")) {
			logger.debug(searchMap);
			bannerList = service.searchWithNoKeywordCaching(searchMap);
			logger.debug(bannerList);
			
			if (bannerList.size() == 0) {
				searchMap.put("type1", "category");
				searchMap.put("keyword1", "ALL");
				logger.debug(searchMap);
				bannerList = service.searchWithNoKeywordCaching(searchMap);
				logger.debug(bannerList);
			}
		}
		
		if (bannerList == null || bannerList.size() == 0) {
			searchMap.put("type1", "category");
			searchMap.put("keyword1", "NONE");
			logger.debug(searchMap);
			bannerList = service.searchWithNoKeywordCaching(searchMap);
			logger.debug(bannerList);
			if (bannerList.size() == 0) {
				searchMap.put("type", "area_code");
				searchMap.put("keyword", "ALL");
				logger.debug(searchMap);
				bannerList = service.searchWithNoKeywordCaching(searchMap);
				logger.debug(bannerList);
			}
		}
		
		for (BannerModel banner : bannerList) {
			String imgFileName = banner.getImgFileName();
			banner.setImgFileName(webHelpService.getUploadFilePath(request, BANNER_UPLOAD_PATH) + imgFileName);
		}
		
		
		return bannerList;
	}

	@RequestMapping("/write.do")
	public ModelAndView write(@ModelAttribute("BannerModel") BannerModel bannerModel){
		
		List<CodeModel> areaList = cachedCodeService.getCodeList("AREA");
		List<CodeModel> categoryList = cachedCodeService.getCodeList("CATEGORY");
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("areaList", areaList);
		mav.addObject("categoryList", categoryList);
		mav.setViewName("/banner/write");
		
		return mav;
	}
	
	@RequestMapping(value="/write.do", method=RequestMethod.POST)
	public String shopWriteProc(@ModelAttribute("BannerModel") BannerModel bannerModel, MultipartHttpServletRequest request){
		
		MultipartFile newImgFile = request.getFile("newImgFile");
		String newImgFileName = StringUtils.trimToEmpty(newImgFile.getOriginalFilename());
		
		if(newImgFile != null && !newImgFileName.equals("")){
			webHelpService.createNewUploadFile(request, BANNER_UPLOAD_PATH, newImgFile, newImgFileName);
		}
		bannerModel.setImgFileName(newImgFileName);
		
		
		bannerModel.setBannerId(service.getNewBannerId());
		logger.debug(bannerModel);
		service.write(bannerModel);		
		
		return "redirect:list.do";
	}
	
	@RequestMapping("/view.do")
	public ModelAndView view(HttpServletRequest request){
		String bannerId = request.getParameter("bannerId");
		
		BannerModel banner = service.getOne(bannerId); // get selected article model
		
		List<CodeModel> areaList = cachedCodeService.getCodeList("AREA");
		List<CodeModel> categoryList = cachedCodeService.getCodeList("CATEGORY");
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("banner", banner);
		mav.addObject("areaList", areaList);
		mav.addObject("categoryList", categoryList);
		mav.setViewName("/banner/view");
		return mav;
	}
	
	@RequestMapping("/modify.do")
	public ModelAndView modify(HttpServletRequest request, HttpSession session){
		String bannerId = request.getParameter("bannerId");
		List<CodeModel> areaList = cachedCodeService.getCodeList("AREA");
		List<CodeModel> categoryList = cachedCodeService.getCodeList("CATEGORY");
		BannerModel banner = service.getOne(bannerId);
		// <br /> tag change to new line code
		//String content = shop.getContent().replaceAll("<br />", "\r\n");
		//shop.setContent(content);
		//
		
		Map<String,String> searchMap = new HashMap<String,String>();
		searchMap.put("type", "area_code");
		searchMap.put("keyword", banner.getAreaCode());
		
		
		List<ShopModel> shopList = shopService.search(searchMap);
		ModelAndView mav = new ModelAndView();
		
		mav.addObject("banner", banner);
		mav.setViewName("/banner/modify");
		mav.addObject("areaList", areaList);
		mav.addObject("categoryList", categoryList);
		mav.addObject("shopList", shopList);
		
		return mav;
	}
	
	@RequestMapping(value = "/modify.do", method=RequestMethod.POST)
	public ModelAndView modifyProc(@ModelAttribute("BannerModel") BannerModel bannerModel, MultipartHttpServletRequest request){
		
		String imgFileName = request.getParameter("imgFileName");
		MultipartFile newImgFile = request.getFile("newImgFile");
		String newImgFileName = newImgFile.getOriginalFilename();
		String imgFileDeleteFlag = request.getParameter("imgFileDeleteFlag");
		
		if(newImgFile != null && !newImgFileName.equals("")){
			webHelpService.deleteOriginalFile(request, BANNER_UPLOAD_PATH, imgFileName);
			webHelpService.createNewUploadFile(request, BANNER_UPLOAD_PATH, newImgFile, newImgFileName);
			//
			bannerModel.setImgFileName(newImgFileName);
		} else if (imgFileDeleteFlag != null && !imgFileDeleteFlag.equals("")) {
			webHelpService.deleteOriginalFile(request, BANNER_UPLOAD_PATH, imgFileName);
			bannerModel.setImgFileName("");
		}
		
		service.modify(bannerModel);
		
		ModelAndView mav = new ModelAndView();
		//mav.addObject("idx", shopModel.getIdx());
		mav.addObject("bannerId", bannerModel.getBannerId());
		mav.setViewName("redirect:/banner/view.do");
		return mav;
	}
	
	@RequestMapping("/delete.do")
	public ModelAndView shopDelete(HttpServletRequest request, HttpSession session){
		String bannerId = request.getParameter("bannerId");
		
		BannerModel banner = service.getOne(bannerId);
		
		ModelAndView mav = new ModelAndView();
		

		// if: when the article has upload file - remove that
		if(banner.getImgFileName() != null){
			webHelpService.deleteOriginalFile(request, BANNER_UPLOAD_PATH, banner.getImgFileName());
		}
		//				
		service.delete(bannerId);
		
		mav.setViewName("redirect:list.do");
		return mav;
	}
}
