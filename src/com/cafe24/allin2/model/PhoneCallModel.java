package com.cafe24.allin2.model;

public class PhoneCallModel {
	private int rnum;
	private int idx; 
	private String shop_code;
	private String phone;
	private String banner_flg;
	private String writeDate;
	  
	public int getRnum() {
		return rnum;
	}
	public void setRnum(int rnum) {
		this.rnum = rnum;
	}
	public int getIdx() {
		return idx;
	}
	public void setIdx(int idx) {
		this.idx = idx;
	}
	public String getShop_code() {
		return shop_code;
	}
	public void setShop_code(String shop_code) {
		this.shop_code = shop_code;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getBanner_flg() {
		return banner_flg;
	}
	public void setBanner_flg(String banner_flg) {
		this.banner_flg = banner_flg;
	}	
	 
	public String getWriteDate() {
		return writeDate;
	}
	public void setWriteDate(String writeDate) {
		this.writeDate = writeDate;
	}
	 
	
	 
}
