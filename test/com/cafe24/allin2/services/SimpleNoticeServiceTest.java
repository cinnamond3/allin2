package com.cafe24.allin2.services;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

import com.cafe24.allin2.model.SimpleNoticeModel;
import com.cafe24.allin2.service.SimpleNoticeService;

public class SimpleNoticeServiceTest {

	
	
	@Test
	public void testGetOne() {
		ApplicationContext context = new GenericXmlApplicationContext("/config/applicationContext.xml");
		SimpleNoticeService simpleNoticeService = (SimpleNoticeService) context.getBean("simpleNoticeService");
		
		SimpleNoticeModel simpleNotice = simpleNoticeService.getOne("");
		System.out.println("simpleNotice > " + simpleNotice);
		System.out.println("simpleNotice > " + simpleNotice.getNotifyLink());
		System.out.println("simpleNotice > " + simpleNotice.getNotifyText());
		System.out.println("simpleNotice > " + simpleNotice.getPopupText());
	}
	
	@Test
	public void testModify() {
		ApplicationContext context = new GenericXmlApplicationContext("/config/applicationContext.xml");
		SimpleNoticeService simpleNoticeService = (SimpleNoticeService) context.getBean("simpleNoticeService");
		
		SimpleNoticeModel simpleNotice = new SimpleNoticeModel();
		simpleNotice.setNotifyLink("link 정보");
		simpleNotice.setNotifyText("한줄공지 내용입니다.");
		simpleNotice.setPopupText("팝업 공지 내용입니다.");
		
		simpleNoticeService.modify(simpleNotice);
		System.out.println("simpleNotice > " + simpleNotice);
		System.out.println("simpleNotice > " + simpleNotice.getNotifyLink());
		System.out.println("simpleNotice > " + simpleNotice.getNotifyText());
		System.out.println("simpleNotice > " + simpleNotice.getPopupText());
	}

}
