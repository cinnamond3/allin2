package com.cafe24.allin2.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.cafe24.allin2.dao.BoardDao;
import com.cafe24.allin2.dao.CallCountHistoryDao;
import com.cafe24.allin2.model.BoardCommentModel;
import com.cafe24.allin2.model.BoardModel;
import com.cafe24.allin2.model.CallCountHistoryModel;

public class CallCountHistoryService extends SqlMapClientDaoSupport implements CallCountHistoryDao {

	private HashMap<String, Object> valueMap = new HashMap<String, Object>();

	@Override
	public List<CallCountHistoryModel> search(Map<String,String> searchMap, int startArticleNum, int endArticleNum) {
		valueMap.putAll(searchMap);
		valueMap.put("startNum", startArticleNum);
		valueMap.put("endNum", endArticleNum);
		return getSqlMapClientTemplate().queryForList("callCountHistory.search", valueMap);
	}
	
	@Override
	public int getSearchTotalNum(Map<String,String> searchMap) {
		valueMap.putAll(searchMap);
		return (Integer) getSqlMapClientTemplate().queryForObject("callCountHistory.getSearchTotalNum", valueMap);
	}

}
