package com.cafe24.allin2.model;

public class MobileUserModel {
	private int rnum; 
	private int idx; 
	private String area_code; 
	private String phone_number;
	private String reg_id;
	private String use_yn;  
	
	public int getRnum() {
		return rnum;
	}
	public void setRnum(int rnum) {
		this.rnum = rnum;
	}
	public int getIdx() {
		return idx;
	}
	public void setIdx(int idx) {
		this.idx = idx;
	}
	public String getArea_code() {
		return area_code;
	}
	public void setArea_code(String area_code) {
		this.area_code = area_code;
	} 
	public String getPhone_number() {
		return phone_number;
	}
	public void setPhone_num(String phone_number) {
		this.phone_number = phone_number;
	} 
	public String getReg_id() {
		return reg_id;
	}
	public void setReg_id(String reg_id) {
		this.reg_id = reg_id;
	}	
	public String getUse_yn() {
		return use_yn;
	}
	public void setUse_yn(String use_yn) {
		this.use_yn = use_yn;
	}	  
	
	 
}
