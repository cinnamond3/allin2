package com.cafe24.allin2.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.cafe24.allin2.dao.BoardDao;
import com.cafe24.allin2.model.BoardCommentModel;
import com.cafe24.allin2.model.BoardModel;

public class BoardService extends SqlMapClientDaoSupport implements BoardDao {

	private HashMap<String, Object> valueMap = new HashMap<String, Object>();

	@Override
	public List<BoardModel> getList(int startArticleNum, int endArticleNum, String dateFormat) {
		valueMap.put("startArticleNum", startArticleNum);
		valueMap.put("endArticleNum", endArticleNum);
		valueMap.put("dateFormat", dateFormat);
		System.out.println("valueMap > " + valueMap);
		System.out.println(valueMap.get("dateFormat"));
		return getSqlMapClientTemplate().queryForList("board.getList", valueMap);
	}
	
	@Override
	public BoardModel getOneArticle(int idx) {
		return (BoardModel) getSqlMapClientTemplate().queryForObject("board.getOneArticle", idx);
	}

	@Override
	public List<BoardModel> search(Map<String,String> searchMap, int startArticleNum, int endArticleNum, String dateFormat, String areaCode, String header) {
		valueMap.putAll(searchMap);
		valueMap.put("startArticleNum", startArticleNum);
		valueMap.put("endArticleNum", endArticleNum);
		valueMap.put("dateFormat", BoardModel.DATE_FORMAT1);
		valueMap.put("areaCode", areaCode);
		valueMap.put("header", header);
		return getSqlMapClientTemplate().queryForList("board.search", valueMap);
	}
	
	@Override
	public List<BoardCommentModel> getCommentList(int idx) {
		return getSqlMapClientTemplate().queryForList("board.getCommentList", idx);
	}

	@Override
	public boolean writeArticle(BoardModel board) {
		getSqlMapClientTemplate().insert("board.writeArticle", board);		
		return true;
	}

	@Override
	public boolean writeComment(BoardCommentModel comment) {
		getSqlMapClientTemplate().insert("board.writeComment", comment);
		return true;
	}

	@Override
	public void updateHitcount(int hitcount, int idx) {
		valueMap.put("hitcount", hitcount);
		valueMap.put("idx", idx);
		getSqlMapClientTemplate().update("board.updateHitcount", valueMap);		
	}

	@Override
	public void updateRecommendCount(int recommendcount, int idx) {
		valueMap.put("recommendcount", recommendcount);
		valueMap.put("idx", idx);
		getSqlMapClientTemplate().update("board.updateRecommendcount", valueMap);
		
	}
	@Override
	public int getTotalNum() {
		return (Integer) getSqlMapClientTemplate().queryForObject("board.getTotalNum");
	}

	@Override
	public int getSearchTotalNum(Map<String,String> searchMap, String areaCode, String header) {
		valueMap.putAll(searchMap);
		valueMap.put("areaCode", areaCode);
		valueMap.put("header", header);
		return (Integer) getSqlMapClientTemplate().queryForObject("board.getSearchTotalNum", valueMap);
	}

	@Override
	public void deleteComment(int idx) {
		getSqlMapClientTemplate().delete("board.deleteComment", idx);
	}
	
	@Override
	public void deleteArticle(int idx) {
		getSqlMapClientTemplate().delete("board.deleteArticle", idx);	
	}

	@Override
	public BoardCommentModel getOneComment(int idx) {
		return (BoardCommentModel) getSqlMapClientTemplate().queryForObject("board.getOneComment", idx);		
	}

	@Override
	public boolean modifyArticle(BoardModel board) {
		getSqlMapClientTemplate().update("board.modifyArticle", board);
		return true;
	}	

}
