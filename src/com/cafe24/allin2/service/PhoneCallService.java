package com.cafe24.allin2.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.SqlMapClientTemplate;

 



import com.cafe24.allin2.dao.PhoneCallDao;
import com.cafe24.allin2.model.PhoneCallModel;

public class PhoneCallService implements PhoneCallDao {
	private SqlMapClientTemplate sqlMapClientTemplate;
	private HashMap<String, Object> valueMap = new HashMap<String, Object>();
		

	public void setSqlMapClientTemplate(SqlMapClientTemplate sqlMapClientTemplate) {
		this.sqlMapClientTemplate = sqlMapClientTemplate;
	}
  
	@Override
	public List<PhoneCallModel> getPhoneCallList(int startPhoneCallNum, int endPhoneCallNum) {
		valueMap.put("startPhoneCallNum", startPhoneCallNum);
		valueMap.put("endPhoneCallNum", endPhoneCallNum);
		return sqlMapClientTemplate.queryForList("call.getPhoneCallList", valueMap);
	}
 
	@Override
	public List<PhoneCallModel> searchPhoneCall(Map<String,String> searchMap, String startDate, String EndDate, int startPhoneCallNum, int endPhoneCallNum) {
		valueMap.putAll(searchMap);
		valueMap.put("startDate", startDate);
		valueMap.put("EndDate", EndDate);
		valueMap.put("startPhoneCallNum", startPhoneCallNum);
		valueMap.put("endPhoneCallNum", endPhoneCallNum);
		return sqlMapClientTemplate.queryForList("call.searchPhoneCall", valueMap);
	}
	
	@Override
	public  boolean moblieRegistPhoneCall (PhoneCallModel phoneCall ) {
	  	sqlMapClientTemplate.insert("call.registPhoneCall", phoneCall);
		return true;
	}
	  
	@Override
	public int getTotalNum() {
		return (Integer) sqlMapClientTemplate.queryForObject("call.getTotalNum");
	}

	@Override
	public int getSearchTotalNum(Map<String,String> searchMap, String startDate, String EndDate ) {
		valueMap.putAll(searchMap);
		valueMap.put("startDate", startDate);
		valueMap.put("EndDate", EndDate);
		return (Integer) sqlMapClientTemplate.queryForObject("call.getSearchTotalNum", valueMap);
	}
	@Override
	public void updateShopCallCount(String shop_code ) 
	{
		valueMap.put("shop_code", shop_code);
		 
		 sqlMapClientTemplate.update("call.updateShopCallCount", valueMap);		
		  
	}
	
	  
	@Override
	public boolean insertShopCallCount(String shop_code ) {
		 valueMap.put("shop_code", shop_code);
		 
		 sqlMapClientTemplate.insert("call.insertShopCallCount", valueMap);		
		 return true;
		  
	}
	
	@Override
	public int getShopCallNum(String shop_code) {
		valueMap.put("shop_code", shop_code);
		return (Integer) sqlMapClientTemplate.queryForObject("call.getShopCallInfo", valueMap);
	}
	@Override
	 
	public  boolean mobileGPSRegist (String provider,String latitude,String longitude ) {
		valueMap.put("provider", provider);
		valueMap.put("latitude", latitude);
		valueMap.put("longitude", longitude);
		
	  	sqlMapClientTemplate.insert("call.mobileGPSRegist", valueMap);
		return true;
	}
}
