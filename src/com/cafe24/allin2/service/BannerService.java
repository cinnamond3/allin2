package com.cafe24.allin2.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.cafe24.allin2.dao.BannerDao;
import com.cafe24.allin2.model.BannerModel;
import com.cafe24.allin2.model.ShopModel;

public class BannerService extends SqlMapClientDaoSupport implements BannerDao {
	
	private Map<String, Object> valueMap = new HashMap<String, Object>();
	
	@Override
	public List<BannerModel> getList() {
		return getSqlMapClientTemplate().queryForList("banner.getList");
	}
	
	public List<BannerModel> search(Map<String,String> searchMap) {
		valueMap.putAll(searchMap);
		return getSqlMapClientTemplate().queryForList("banner.search", valueMap );
	}
	
	public List<BannerModel> searchWithNoKeywordCaching(Map<String,String> searchMap) {
		return getSqlMapClientTemplate().queryForList("banner.search", searchMap );
	}
	
	@Override
	public BannerModel getOne(String bannerId) {
		
		return (BannerModel) getSqlMapClientTemplate().queryForObject("banner.getOne", bannerId);
	}

	@Override
	public boolean modify(BannerModel bannerModel) {
		getSqlMapClientTemplate().update("banner.modify", bannerModel);
		return true;
		
	}

	@Override
	public String getNewBannerId() {
		return (String)getSqlMapClientTemplate().queryForObject("banner.getNewBannerId");
	}

	@Override
	public boolean write(BannerModel banner) {
		getSqlMapClientTemplate().insert("banner.write", banner);
		return true;
		
	}

	@Override
	public boolean delete(String bannerId) {
		getSqlMapClientTemplate().delete("banner.delete", bannerId);
		return true;
		
	}

		

	
}
