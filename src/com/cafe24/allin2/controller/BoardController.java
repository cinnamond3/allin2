package com.cafe24.allin2.controller;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import sun.font.CreatedFontTracker;

import com.cafe24.allin2.helpservice.WebHelpService;
import com.cafe24.allin2.model.BoardCommentModel;
import com.cafe24.allin2.model.BoardModel;
import com.cafe24.allin2.model.CodeModel;
import com.cafe24.allin2.service.BoardService;
import com.cafe24.allin2.service.CachedCodeService;

@Controller
@RequestMapping("/board")
public class BoardController {
	// DI
	private ApplicationContext context = new GenericXmlApplicationContext("/config/applicationContext.xml");
	private BoardService service = (BoardService) context.getBean("boardService");
	
	
	private CachedCodeService cachedCodeService = new CachedCodeService();
	private WebHelpService webHelpService = new WebHelpService();
	
	Logger logger = Logger.getLogger(BoardController.class);
	//
	// * User variable
	// article, page variables
	private int currentPage = 1;
	private static final int showArticleLimit = 10; // change value if want to show more articles by one page
	private static final int showPageLimit = 10; // change value if want to show more page links
	private static final int showArticleLimitForMobile = 20; // change value if want to show more articles by one page
	private static final int showPageLimitForMobile = 10; // change value if want to show more page links
	private int startArticleNum = 0;
	private int endArticleNum = 0;
	private int totalNum = 0;
	//
	
	@RequestMapping("/mobile/list.do")
	public ModelAndView boardListForMobile(HttpServletRequest request, HttpServletResponse response){
		
		Map<String,Object> data = getPageHtml(request, showArticleLimitForMobile, showPageLimitForMobile, BoardModel.DATE_FORMAT2);
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("boardList", data.get("boardList"));
		mav.addObject("pageHtml", data.get("pageHtml"));
		mav.setViewName("/board/mobile/list");
		
		return mav;
	}
	
	@RequestMapping("/list.do")
	public ModelAndView boardList(HttpServletRequest request, HttpServletResponse response){
		
		Map<String,Object> data = getPageHtml(request, showArticleLimit, showPageLimit, BoardModel.DATE_FORMAT1);
		List<CodeModel> areaList = cachedCodeService.getCodeList("AREA");
		List<CodeModel> headerList = cachedCodeService.getCodeList("HEADER");
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("boardList", data.get("boardList"));
		mav.addObject("headerList", headerList);
		mav.addObject("areaList", areaList);
		mav.addObject("pageHtml", data.get("pageHtml"));
		mav.setViewName("/board/list");
		
		return mav;
	}
	
	public Map<String,Object> getPageHtml(HttpServletRequest request, int showArticleLimit, int showPageLimit, String dateFormat){
		
		// set variables from request parameter
		if(request.getParameter("page") == null || request.getParameter("page").trim().isEmpty() || request.getParameter("page").equals("0")) {
			currentPage = 1;
		} else {
			currentPage = Integer.parseInt(request.getParameter("page"));
		}
		
		String areaCode = StringUtils.trimToEmpty(request.getParameter("areaCode"));
		String header = StringUtils.trimToEmpty(request.getParameter("header"));
		//
		
		// expression article variables value
		startArticleNum = (currentPage - 1) * showArticleLimit + 1;
		endArticleNum = startArticleNum + showArticleLimit -1;
		//
		
		// get boardList and get page html code
		List<BoardModel> boardList;
		Map<String,String> searchMap = webHelpService.getSearchMap(request);
		if(webHelpService.isSearch(searchMap) || !areaCode.equals("") || !header.equals("")){
			boardList = service.search(searchMap, startArticleNum, endArticleNum, dateFormat, areaCode, header);
			totalNum = service.getSearchTotalNum(searchMap, areaCode, header);
		} else {
			boardList = service.getList(startArticleNum, endArticleNum, dateFormat);
			totalNum = service.getTotalNum();
		}
		StringBuffer pageHtml = webHelpService.getPageHtml(currentPage, totalNum, showArticleLimit, showPageLimit, searchMap);
		
		Map<String,Object> data = new HashMap<String,Object>();
		data.put("boardList", boardList);
		data.put("pageHtml", pageHtml);
		
		return data;
	}


	@RequestMapping("/view.do")
	public ModelAndView boardView(HttpServletRequest request){
		int idx = Integer.parseInt(request.getParameter("idx"));		
		BoardModel board = service.getOneArticle(idx); // get selected article model
		service.updateHitcount(board.getHitcount()+1, idx); // update hitcount
		
		List<BoardCommentModel> commentList = service.getCommentList(idx); // get comment list
		List<CodeModel> areaList = cachedCodeService.getCodeList("AREA");
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("board", board);
		mav.addObject("commentList", commentList);
		mav.addObject("areaList", areaList);
		mav.setViewName("/board/view");
		return mav;
	}
	
	@RequestMapping("/mobile/view.do")
	public ModelAndView boardView2(HttpServletRequest request){
		int idx = Integer.parseInt(request.getParameter("idx"));		
		BoardModel board = service.getOneArticle(idx); // get selected article model
		service.updateHitcount(board.getHitcount()+1, idx); // update hitcount
		
		List<BoardCommentModel> commentList = service.getCommentList(idx); // get comment list
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("board", board);
		mav.addObject("commentList", commentList);
		mav.setViewName("/board/mobile/view");
		return mav;
	}
	
	@RequestMapping("/write.do")
	public ModelAndView boardWrite(@ModelAttribute("BoardModel") BoardModel boardModel){
		
		List<CodeModel> areaList = cachedCodeService.getCodeList("AREA");
		List<CodeModel> headerList = cachedCodeService.getCodeList("HEADER");
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("headerList", headerList);
		mav.addObject("areaList", areaList);
		mav.setViewName("/board/write");
		return mav;
	}
	
	@RequestMapping(value="/write.do", method=RequestMethod.POST)
	public String boardWriteProc(@ModelAttribute("BoardModel") BoardModel boardModel, MultipartHttpServletRequest request){
		
		// get upload file
		MultipartFile newFile = request.getFile("file");
		String newFileName = newFile.getOriginalFilename();
		
		webHelpService.createNewUploadFile(request, "file", newFile, newFileName);
		boardModel.setFileName(newFileName);
		//
		// new line code change to <br /> tag	
		String content =  boardModel.getContent().replaceAll("\r\n", "<br />");		
		boardModel.setContent(content);
		//
		service.writeArticle(boardModel);		
		
		return "redirect:list.do";
	}


	
	
	@RequestMapping("/commentWrite.do")
	public ModelAndView commentWriteProc(@ModelAttribute("CommentModel") BoardCommentModel commentModel){
		// new line code change to <br /> tag
		String content = commentModel.getContent().replaceAll("\r\n", "<br />");
		commentModel.setContent(content);
		//
		service.writeComment(commentModel);
		ModelAndView mav = new ModelAndView();
		mav.addObject("idx", commentModel.getLinkedArticleNum());
		mav.setViewName("redirect:view.do");
		
		return mav;
	}
	
	@RequestMapping("/modify.do")
	public ModelAndView boardModify(HttpServletRequest request, HttpSession session){
		String userId = (String) session.getAttribute("userId");
		int idx = Integer.parseInt(request.getParameter("idx"));
		
		BoardModel board = service.getOneArticle(idx);
		// <br /> tag change to new line code
		String content = board.getContent().replaceAll("<br />", "\r\n");
		board.setContent(content);
		
		
		List<CodeModel> areaList = cachedCodeService.getCodeList("AREA");
		List<CodeModel> headerList = cachedCodeService.getCodeList("HEADER");
		//
		
		ModelAndView mav = new ModelAndView();
		
		if(!userId.equals(board.getWriterId())){
			mav.addObject("errCode", "1");	// forbidden connection
			mav.addObject("idx", idx);
			mav.setViewName("redirect:view.do");
		} else {
			mav.addObject("board", board);
			mav.addObject("headerList", headerList);
			mav.addObject("areaList", areaList);
			mav.setViewName("/board/modify");
		}		
		
		return mav;
	}
	
	@RequestMapping(value = "/modify.do", method=RequestMethod.POST)
	public ModelAndView boardModifyProc(@ModelAttribute("BoardModel") BoardModel boardModel, MultipartHttpServletRequest request){
		String orgFileName = request.getParameter("orgFile");
		MultipartFile newFile = request.getFile("newFile");
		String deleteFlag = request.getParameter("deleteFlag");
		
		logger.debug("deleteFlag > " + deleteFlag);
		
		String newFileName = newFile.getOriginalFilename();
		
		boardModel.setFileName(orgFileName);
		
		// if: when want to change upload file
		if(newFile != null && !newFileName.equals("")){
			webHelpService.deleteOriginalFile(request, "files", orgFileName);
			webHelpService.createNewUploadFile(request, "files", newFile, newFileName);
			//
			boardModel.setFileName(newFileName);
		} else if (deleteFlag != null && !deleteFlag.equals("")) {
			webHelpService.deleteOriginalFile(request, "files", orgFileName);
			boardModel.setFileName("");
		}
		//
		// new line code change to <br /> tag
		String content =  boardModel.getContent().replaceAll("\r\n", "<br />");		
		boardModel.setContent(content);
		//
		
		service.modifyArticle(boardModel);
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("idx", boardModel.getIdx());
		mav.setViewName("redirect:/board/view.do");
		return mav;
	}

		
	@RequestMapping("/delete.do")
	public ModelAndView boardDelete(HttpServletRequest request, HttpSession session){
		String userId = (String) session.getAttribute("userId");
		int idx = Integer.parseInt(request.getParameter("idx"));
		
		BoardModel board = service.getOneArticle(idx);
		
		ModelAndView mav = new ModelAndView();
		
		if(!userId.equals(board.getWriterId())){
			mav.addObject("errCode", "1");	// it's forbidden connection
			mav.addObject("idx", idx);
			mav.setViewName("redirect:view.do");
		} else {
			List<BoardCommentModel> commentList = service.getCommentList(idx); // check comments
			if(commentList.size() > 0){
				mav.addObject("errCode", "2"); // can't delete because a article has comments
				mav.addObject("idx", idx);
				mav.setViewName("redirect:view.do");
			} else {
				// if: when the article has upload file - remove that
				if(board.getFileName() != null){
					webHelpService.deleteOriginalFile(request, "file", board.getFileName());
				}
				//				
				service.deleteArticle(idx);
				
				mav.setViewName("redirect:list.do");
			}
		}		
		return mav;
	}
	
	@RequestMapping("/commentDelete.do")
	public ModelAndView commendDelete(HttpServletRequest request, HttpSession session){
		int idx = Integer.parseInt(request.getParameter("idx"));
		int linkedArticleNum = Integer.parseInt(request.getParameter("linkedArticleNum"));
		
		String userId = (String) session.getAttribute("userId");
		BoardCommentModel comment = service.getOneComment(idx);
		
		ModelAndView mav = new ModelAndView();
		
		if(!userId.equals(comment.getWriterId())){
			mav.addObject("errCode", "1");
		} else {
			service.deleteComment(idx);
		}		
				
		mav.addObject("idx", linkedArticleNum); // move back to the article
		mav.setViewName("redirect:view.do");
		
		return mav;
	}
	
	@RequestMapping("/recommend.do")
	public ModelAndView updateRecommendcount(HttpServletRequest request, HttpSession session){
		int idx = Integer.parseInt(request.getParameter("idx"));
		String userId = (String) session.getAttribute("userId");
		BoardModel board = service.getOneArticle(idx);
		
		ModelAndView mav = new ModelAndView();
		
		if(userId.equals(board.getWriterId())){
			mav.addObject("errCode", "1");
		} else {
			service.updateRecommendCount(board.getRecommendcount()+1, idx);
		}
		
		mav.addObject("idx", idx);
		mav.setViewName("redirect:/board/view.do");
		
		return mav;
	}
}
