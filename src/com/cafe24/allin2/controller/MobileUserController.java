package com.cafe24.allin2.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;












import com.cafe24.allin2.helpservice.WebHelpService;
import com.cafe24.allin2.model.CodeModel;
import com.cafe24.allin2.model.MobileUserModel;
import com.cafe24.allin2.service.MobileUserService;
import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;

@Controller
@RequestMapping("/mobileSync")
public class MobileUserController {
	// DI
	private ApplicationContext context = new GenericXmlApplicationContext("/config/applicationContext.xml");
	private MobileUserService service = (MobileUserService) context.getBean("mobileUserService");
	private WebHelpService webHelpService = new WebHelpService();
	//
	// * User variable
	// article, page variables
	private int currentPage = 1;
	private int showMobileUserLimit = 10; // change value if want to show more articles by one page
	private int showPageLimit = 10; // change value if want to show more page links
	private int startMobileUserNum = 0;
	private int endMobileUserNum = 0;
	private int totalNum = 0;
	private int text=0;
	  
	@RequestMapping(value = "/mobile/regIdRegist.do", method=RequestMethod.POST)
	//public ModelAndView mobileUserRegist(@ModelAttribute("MobileUserModel") MobileUserModel mobileUserModel, MultipartHttpServletRequest request){
	public ModelAndView mobileUserRegist(HttpServletRequest request, HttpServletResponse response){
		 
		ModelAndView mav = new ModelAndView();
		String phone_number ="";
		
		try
		{
			 
			MobileUserModel mobileUserModel = new MobileUserModel();
			String reg_Id="";
			String area_code="";
			String use_yn=""; 
			phone_number = request.getParameter("phone_number") ;
			
			reg_Id = request.getParameter("reg_id") ;
			area_code = request.getParameter("area_code") ;
			use_yn = request.getParameter("use_flg") ;
			
			MobileUserModel mobileUserModel_1 = service.getMobileUserOne(phone_number);
			
			if (mobileUserModel_1 != null)
			{
				 
				if ( mobileUserModel_1.getArea_code().equals(area_code) == false )
				{
					 
					service.updateMobileUser(phone_number, area_code, reg_Id, use_yn  );
				}
				else if ( mobileUserModel_1.getReg_id().equals(reg_Id) == false )
				{
					 
					service.updateMobileUser(phone_number, area_code, reg_Id, use_yn );
				}
				else if ( mobileUserModel_1.getUse_yn().equals(use_yn) == false )
				{
					 
					service.updateMobileUser(phone_number, area_code, reg_Id, use_yn );
				}
				
			}
			else
			{
				mobileUserModel.setArea_code(area_code);
				mobileUserModel.setPhone_num(phone_number); 
				mobileUserModel.setReg_id(reg_Id);
				mobileUserModel.setUse_yn(use_yn);
				
				service.moblieRegistMobileUser(mobileUserModel );
			}
			 
			mav.addObject("result", "OK");
			 
			
			 
			mav.setViewName("/mobileSync/mobile/regIdRegist");
			return mav;
		}
		catch (Exception e) {
			
			mav.addObject("result", "NG");
			 
			mav.setViewName("/mobileSync/mobile/regIdRegist");
			return mav;
		}
	}
	
	@RequestMapping(value = "/GCM_Send.do", method=RequestMethod.POST)
	public ModelAndView gcmMobileSend(HttpServletRequest request, HttpServletResponse response){
		 
		ModelAndView mav = new ModelAndView();
		Sender sender;
		
		try
		{
			String subject =""; 
			 
			String content ="";  
			 
			String area_code="";
			 
			subject = request.getParameter("subject") ;
			
			content = request.getParameter("content") ;
			area_code = request.getParameter("area_code") ;
			
			Map<String,String> searchMap = new HashMap<String,String>();
			searchMap.put("type", "AREA_CODE");
			searchMap.put("keyword", area_code);
			
			List<MobileUserModel> mobileUserList;
			if(webHelpService.isSearch(searchMap)){
				mobileUserList = service.searchMobileUser(searchMap, startMobileUserNum, endMobileUserNum);
			
			}
			else
			{
				mobileUserList = service.getMobileUserList(startMobileUserNum, endMobileUserNum);
			}
			
			String apiKey2="AIzaSyCVrh5IgIeUMFG8V4wkQf8kHilSBMH8qIY";
			sender = new Sender(apiKey2);
			
			Message message = new Message.Builder().addData("title",subject).addData("msg",content)
					.build();
 
			
			/* 건별 처리 */
		 
			for ( int i =0 ; i < mobileUserList.size() ; i++)
			{
				try
				{
					Result result = sender.send(message, mobileUserList.get(i).getReg_id(), 5);
  
				}
				catch(Exception e)
				{
					
				}
			}
			 
			/* 배열 처리 */
			 /*
			List<String> list = new ArrayList<String>();
			for ( int i =0 ; i < mobileUserList.size() ; i++)
			{
				list.add(mobileUserList.get(i).getReg_id());
				
			}
			
			MulticastResult multiResult = sender.send(message, list, 5);

			if (multiResult != null) {

				List resultList = multiResult.getResults();


			 
 
				for (Result result1 : resultList) {

					System.out.println(result1.getMessageId());

				}
			 

			}

			 */
  
			 
			mav.setViewName("redirect:list.do");
			
			return mav; 
		}
		catch (Exception e) {
			
			mav.setViewName("redirect:list.do");
			
			return mav; 
		}
	}
	
	
	@RequestMapping("/list.do")
	public ModelAndView mobileUserList(HttpServletRequest request, HttpServletResponse response){
		
			 
		// set variables from request parameter
		if(request.getParameter("page") == null || request.getParameter("page").trim().isEmpty() || request.getParameter("page").equals("0")) {
			currentPage = 1;
		} else {
			currentPage = Integer.parseInt(request.getParameter("page"));
		}
		
		Map<String,String> searchMap = webHelpService.getSearchMap(request);

		// expression article variables value
		startMobileUserNum = (currentPage - 1) * showMobileUserLimit + 1;
		endMobileUserNum = startMobileUserNum + showMobileUserLimit -1;
		//
		
		// get mobileUserList and get page html code
		List<MobileUserModel> mobileUserList;
		if(webHelpService.isSearch(searchMap)){
			mobileUserList = service.searchMobileUser(searchMap, startMobileUserNum, endMobileUserNum);
			totalNum = service.getSearchTotalNum(searchMap);
		} else {
			mobileUserList = service.getMobileUserList(startMobileUserNum, endMobileUserNum);
			totalNum = service.getTotalNum();
		}
		
		StringBuffer pageHtml = webHelpService.getPageHtml(currentPage, totalNum, showMobileUserLimit, showPageLimit, searchMap);
		//
		
	    searchMap.put("type", "CODE_NAME");
	    searchMap.put("keyword", "AREA");
	    
		List<CodeModel> CodeList = service.searchAreaCode(searchMap);
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("mobileUserList", mobileUserList);
		mav.addObject("codeList", CodeList);
		mav.addObject("pageHtml", pageHtml);
		mav.setViewName("/mobileSync/list");
		
		return mav;
	}
}
