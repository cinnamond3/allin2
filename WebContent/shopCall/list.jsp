<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>전화 히스토리 목록 보기</title>
<link href="<%=request.getContextPath()%>/css/common.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript">
<!--
	function selectedOptionCheck(){
		$("#type > option[value=<%=request.getParameter("type")%>]").attr("selected", "true");
	}
	
	function moveAction(where){
		switch (where) {
		case 2:
			location.href = "list.do";
			break;
		}
	}
//-->
</script>
</head>
<body onload="selectedOptionCheck()">
<div class="wrapper">
	<div class="nav">
		<jsp:include page="/common/menu.jsp"></jsp:include>
	</div>
	<div class="header">
		<form action="list.do" method="get">		
			년월 <input type="hidden" id="type" name="type" value="call_month" />
			: <input type="text" id="keyword" name="keyword" value="<%=StringUtils.trimToEmpty(request.getParameter("keyword"))%>" />
			업체코드 : <input type="hidden" id="type1" name="type1" value="shop_code" />
			: <input type="text" id="keyword1" name="keyword1" value="<%=StringUtils.trimToEmpty(request.getParameter("keyword1"))%>" />
			<input type="submit" value="검색" />
			<br/>
		</form>
	</div>	
	
	<table border="0" class="view">
	   <colgroup>
	       <col width="10%"/>
	       <col width="10%"/>
	       <col width="50%"/>
	       <col width="10%"/>
	       <col width="10%"/>
	       <col width="10%"/>
	   </colgroup>
		<thead>
		<tr>
			<th>년월</th>
			<th>업체코드</th>
			<th>업체명</th>
			<th>전화횟수</th>
			<th>검색횟수</th>
			<th>배너클릭횟수</th>
		</tr>
		</thead>
		<tbody>
		<c:forEach var="shopCallItem" items="${shopCallList}">
		<tr>
			<td>${shopCallItem.callMonth}</td>
			<td>${shopCallItem.shopCode}</td>
			<td>${shopCallItem.shopName}</td>
			<td>${shopCallItem.callCount}</td>
			<td>${shopCallItem.searchCount}</td>		
			<td>${shopCallItem.bannerCount}</td>		
		</tr>
		</c:forEach>
		</tbody>
	</table>
	<br />
	${pageHtml}
	<br /><br />
	<input type="button" value="목록" class="writeBt" onclick="moveAction(2)"/>
	<input type="button" value="쓰기" class="writeBt" onclick="moveAction(1)"/>	
</div>
</body>
</html>