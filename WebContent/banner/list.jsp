<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>게시판 글 목록 보기</title>
<link href="<%=request.getContextPath()%>/css/banner.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript">
<!--
	function selectedOptionCheck(){
		$("#type > option[value=<%=request.getParameter("type")%>]").attr("selected", "true");
	}
	
	function view(index){
		location.href = "view.do?bannerId="+index;
	}
	
	function moveAction(where){
		switch (where) {
		case 1:
			location.href = "write.do";
			break;
		
		case 2:
			location.href = "list.do";
			break;
		}
	}
//-->
</script>
</head>
<body onload="selectedOptionCheck()">
<div class="wrapper">
	<div class="nav">
		<jsp:include page="/common/menu.jsp"></jsp:include>
	</div>
	<div class="header">
		<form action="list.do" method="get">		
			<select id="type" name="type">
				<option value="area_code">지역코드</option>
			</select>
			<input type="text" id="keyword" name="keyword" value="<%if(request.getParameter("keyword") != null){ out.print(request.getParameter("keyword")); } else { out.print(""); }%>" />
			<input type="submit" value="검색" />			
			<select id="type" name="type1">
				<option value="category">카테고리</option>
			</select>
			<input type="text" id="keyword1" name="keyword1" value="<%if(request.getParameter("keyword1") != null){ out.print(request.getParameter("keyword1")); } else { out.print(""); }%>" />
			<input type="submit" value="검색" />			
		</form>
	</div>	
	
	<table border="0">
		<colgroup>
		<col width="10%"/>
		<col width="10%"/>
		<col width="10%"/>
		<col width="30%"/>
		<col width="10%"/>
		<col width="30%"/>
		</colgroup>
		<thead>
		<tr>
			<th>배너 ID</th>
			<th>지역</th>
			<th>카테고리</th>
			<th>배너이미지</th>
			<th>업체코드</th>
			<th>Web Link 주소</th>
		</tr>
		</thead>
		<tbody>
		<c:forEach var="banner" items="${bannerList}">
		<tr onclick="view(${banner.bannerId})" onmouseover="this.className='mouseover'" onmouseout="this.className=''">
			<td>${banner.bannerId}</td>
			<td>
                <c:forEach var="areaItem" items="${areaList}">
                    <c:if test="${areaItem.code_value == banner.areaCode}">${areaItem.code_desc}</c:if>
                </c:forEach>
            </td>
            <td>
                <c:forEach var="categoryItem" items="${categoryList}">
                    <c:if test="${categoryItem.code_value == banner.category}">${categoryItem.code_desc}</c:if>
                </c:forEach>
            </td>
			<td>${banner.imgFileName}</td>
			<td>${banner.shopCode}</td>
			<td>${banner.webUrl}</td>	
		</tr>
		</c:forEach>
		</tbody>
	</table>
	<br />
	${pageHtml}
	<br /><br />
	<input type="button" value="목록" class="writeBt" onclick="moveAction(2)"/>
	<input type="button" value="쓰기" class="writeBt" onclick="moveAction(1)"/><br/>
	<span>* 카테고리별 배너가 없는 경우 카테고리별 기본배너가 표시됩니다.</span><br/>
	<span>* 메인화면 배너에서 지역별 배너가 없는 경우 메인기본배너가 표시됩니다.</span>
		
</div>
</body>
</html>