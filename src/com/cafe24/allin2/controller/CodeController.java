package com.cafe24.allin2.controller;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.cafe24.allin2.helpservice.WebHelpService;
import com.cafe24.allin2.model.CodeModel;
import com.cafe24.allin2.service.CodeService;

@Controller
@RequestMapping("/code")
public class CodeController {
	// DI
	private ApplicationContext context = new GenericXmlApplicationContext("/config/applicationContext.xml");
	private CodeService service = (CodeService) context.getBean("codeService");
	private WebHelpService webHelpService = new WebHelpService();
	//
	// * User variable
	// Code, page variables
	private int currentPage = 1;
	private int showCodeLimit = 10; // change value if want to show more Codes by one page
	private int showPageLimit = 10; // change value if want to show more page links
	private int startCodeNum = 0;
	private int endCodeNum = 0;
	private int totalNum = 0;
	//
	// file upload path
	private String uploadPath = "D:/Project/workspace/SummerCode/WebContent/files/";
	//
	//
	
	@RequestMapping("/mobile/list.do")
	public ModelAndView MobileCodeList(HttpServletRequest request, HttpServletResponse response){
		
		Map<String,String> searchMap = webHelpService.getSearchMap(request);  
		 
		// get CodeList and get page html code
		List<CodeModel> CodeList;
		if(webHelpService.isSearch(searchMap)){
			CodeList = service.moblieSearchCode(searchMap);
		} 
		else {
			int istartCodeNum = 1;
			int iendCodeNum = 99;
			
			CodeList = service.getCodeList(istartCodeNum, iendCodeNum);
			totalNum = service.getTotalNum();
		}
		 
		 
		 
		JSONArray childNodes = new JSONArray();
		JSONObject childNode = new JSONObject();

		CodeModel lCodeModel;
		 
	    for (int i = 0 ; i < CodeList.size(); i++)
		{
			lCodeModel = (CodeModel)CodeList.get(i);
			
			childNode = new JSONObject();
			childNode.put( "code_name", lCodeModel.getCode_name());
			childNode.put( "code_value", lCodeModel.getCode_value());
			childNode.put( "code_desc", lCodeModel.getCode_desc());
			
			childNodes.add(childNode);
			
		}
		  
		JSONObject rootNode = new JSONObject();
		rootNode.put("code_info", childNodes);
  //�׽�Ʈ 
		String data= rootNode.toString();
		 
		 
		ModelAndView mav = new ModelAndView();
		mav.addObject("data", data);
		mav.setViewName("/code/mobile/list");
		 
		return mav; 
	}
	
	 
	@RequestMapping("/list.do")
	public ModelAndView CodeList(HttpServletRequest request, HttpServletResponse response){
		
		// set variables from request parameter
		if(request.getParameter("page") == null || request.getParameter("page").trim().isEmpty() || request.getParameter("page").equals("0")) {
			currentPage = 1;
		} else {
			currentPage = Integer.parseInt(request.getParameter("page"));
		}
		   
		Map<String,String> searchMap = webHelpService.getSearchMap(request);
		//
		
		// expression Code variables value
		startCodeNum = (currentPage - 1) * showCodeLimit + 1;
		endCodeNum = startCodeNum + showCodeLimit -1;
		//
		
		// get CodeList and get page html code
		List<CodeModel> CodeList;
		if(webHelpService.isSearch(searchMap)){
			CodeList = service.searchCode(searchMap, startCodeNum, endCodeNum);
		 	totalNum = service.getSearchTotalNum(searchMap);
		} else {
			CodeList = service.getCodeList(startCodeNum, endCodeNum);
			totalNum = service.getTotalNum();
		}
		StringBuffer pageHtml = webHelpService.getPageHtml(currentPage, totalNum, showCodeLimit, showPageLimit, searchMap);
		//
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("CodeList", CodeList);
		mav.addObject("pageHtml", pageHtml);
		mav.setViewName("/code/list");
		
		return mav;
	}
	
	@RequestMapping("/view.do")
	public ModelAndView CodeView(HttpServletRequest request){
		
		Map<String,String> searchMap = webHelpService.getSearchMap(request);
		
		ModelAndView mav = new ModelAndView();
		if(webHelpService.isSearch(searchMap)){
			
			CodeModel code = service.getOneCode(searchMap);
			mav.addObject("code", code);
			mav.setViewName("/code/view");
		} 
		else {
			mav.setViewName("/code/view");
		}
		 
		return mav;
		 
	}
	 
	
	@RequestMapping("/write.do")
	public String CodeWrite(@ModelAttribute("CodeModel") CodeModel CodeModel){		
		return "/code/write";
	}
	
	@RequestMapping(value="/write.do", method=RequestMethod.POST)
	public String CodeWriteProc(@ModelAttribute("CodeModel") CodeModel CodeModel, MultipartHttpServletRequest request){
		// get upload file
	 
		//
		// new line code change to <br /> tag	
		 
		//
		service.writeCode(CodeModel);		
		
		return "redirect:list.do";
	}
	 
	
	 
	
	@RequestMapping(value = "/modify.do", method=RequestMethod.POST)
	public ModelAndView CodeModifyProc(@ModelAttribute("CodeModel") CodeModel CodeModel, MultipartHttpServletRequest request){
		 
		
		service.modifyCode(CodeModel);
		
		ModelAndView mav = new ModelAndView();
		 
		mav.setViewName("redirect:/code/view.do");
		return mav;
	}
	
	@RequestMapping(value = "/delete.do", method=RequestMethod.POST)
	public ModelAndView CodeDelete(@ModelAttribute("CodeModel") CodeModel CodeModel, MultipartHttpServletRequest request){
		
		service.deleteCode(CodeModel);
		
		ModelAndView mav = new ModelAndView();
		  
				 
		mav.setViewName("redirect:list.do");
		 		
		return mav;
	}
	
	  
	
}
