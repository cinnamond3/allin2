package com.cafe24.allin2.dao;

import java.util.List;
import java.util.Map;

import com.cafe24.allin2.model.CallCountHistoryModel;

public interface CallCountHistoryDao {

	List<CallCountHistoryModel> search(Map<String, String> searchMap,
			int startArticleNum, int endArticleNum);

	int getSearchTotalNum(Map<String, String> searchMap);

}
