<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>글 수정: ${board.subject}</title>
<link href="<%=request.getContextPath()%>/css/simpleNotice.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript">
	function writeFormCheck() {
		var notifyText = $("#notifyText").val();
		
	    if(notifyText != null && notifyText.length > 40){
            alert("40자 이하로 입력하세요! 현재글자수:"+notifyText.length);
            $("#notifyText").focus();
            return false;
        }
	      
		return true;
	}
	
	function moveAction(where){
		switch (where) {
		case 1:
			location.href ="list.do";
			break;
		}
	}
	
	function changeEvent(notifyLinkObj){
		document.getElementById("notifyLink").value = notifyLinkObj.value;
	}
</script>
</head>
<body>
<div class="wrapper">
    <div class="nav">
        <jsp:include page="/common/menu.jsp"></jsp:include>
    </div>  	
	<h3>수정</h3>
	<form action="modify.do" method="post" onsubmit="return writeFormCheck()" enctype="multipart/form-data">
	
	   <table class="write">
        <colgroup>
          <col width="20%">
          <col width="80%">
        </colgroup>
        <tr>
            <th>지역</th>
            <td>
	            <c:forEach var="areaItem" items="${areaList}">
	                <c:if test="${areaItem.code_value == simpleNotice.areaCode}">${areaItem.code_desc}</c:if>
	            </c:forEach>
	            <input type="hidden" id="areaCode" name="areaCode" value="${simpleNotice.areaCode}">
            </td>
        </tr>
        <tr>
            <th>APP 메인 한줄공지</th>
            <td><textarea id="notifyText" name="notifyText" size="40" class="simpleNotice">${simpleNotice.notifyText}</textarea></td>
        </tr>
        <tr>
            <th rowspan="2">APP 메인 한줄공지링크</th>
            <td>
            <select id="selectNotifyLink" name=selectNotifyLink onchange="changeEvent(this)">
            <c:forEach var="boardItem" items="${boardList}">
                <option value="">선택하세요</option>
                <option value="<c:out value="${boardItem.idx }"/>" 
                    <c:if test="${boardItem.idx == shop.idx}">selected="selected"</c:if>>
                    <c:out value="[${boardItem.header}][${boardItem.idx}] ${boardItem.subject} ${boardItem.writeDate}" />
                </option>
            </c:forEach>
            </select>
            </td>
        </tr>
        <tr>
            <td><input type="text" id="notifyLink" name="notifyLink" class="norifyLink" value="${simpleNotice.notifyLink}"/>
            <br/>
            <span>
            * 같은 지역에 해당되는 최근글 100개가 선택박스에서 조회됩니다.<br/>
            * 직접 URL을 입력하시면 그 링크로 바로 연결됩니다.
            </span>
            </td>
        </tr>
        <tr>
            <th>APP 메인 팝업공지</th>
            <td><textarea id="popupText" name="popupText" class="simpleNotice">${simpleNotice.popupText}</textarea></td>
        </tr>
    </table>    
    	
	<br />
	<input type="reset" value="재작성" class="writeBt"/>
	<input type="submit" value="확인" class="writeBt"/>	
	<input type="button" value="목록" class="writeBt" onclick="moveAction(1)" />
	</form>
</div>
</body>
</html>