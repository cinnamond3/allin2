<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="org.springframework.context.annotation.CommonAnnotationBeanPostProcessor"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">
<title>글 보기: ${board.subject}</title>
<link href="<%=request.getContextPath()%>/css/board.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath()%>/css/Style-ETC.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">
	function moveAction(where){
		<% 
		%>
		var areaCode = "<%= StringUtils.trimToEmpty(request.getParameter("areaCode"))%>"; 
		var header = "<%=StringUtils.trimToEmpty(request.getParameter("header"))%>";
		
		var param = "";
		if (areaCode != "") {
			param += "areaCode="+areaCode;
		}
		if (header != "") {
			if (param != "") {
	            param += "&";
	        }
			param += "header="+header;
		}
		if (param != "") {
            param = "?" + param;
        }
    switch (where) {
        case 3:
            location.href = "list.do" + param;
            break;
        }
    }
</script>
</head>
<body onload="">
	
<div id="ct">
	<div class="post_tit">
		<h2>${board.subject}</h2>
		<p class="if">
			
			<span class="if_inr">
           		<span class="im">
				    <span class="hc">작성자</span>
				    <span>${board.writer}</span>
					<span class="bar">|</span>
					<span class="hc">작성일</span>
					<span>${board.writeDate}</span>
				</span>
            <span class="nowp">
                <span class="bar">|</span>
                <span> 조회${board.hitcount}</span>
              		 
            </span> 
		</p>

		<div id="postContent" class="post_cont">
			<p>
				${board.content}
			</p>
				
		</div>
		
	</div>
	<div class="post_ext4">
	<input type="button" value="목록으로" class="writeBt" onclick="moveAction(3)" />
	</div>		
</div>
		
 
</body>
</html>