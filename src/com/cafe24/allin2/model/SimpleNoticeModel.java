package com.cafe24.allin2.model;

public class SimpleNoticeModel {
	private String areaCode;
	private String notifyText;
	private String notifyLink;
	private String popupText;
	
	
	
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public String getNotifyText() {
		return notifyText;
	}
	public void setNotifyText(String notifyText) {
		this.notifyText = notifyText;
	}
	public String getNotifyLink() {
		return notifyLink;
	}
	public void setNotifyLink(String notifyLink) {
		this.notifyLink = notifyLink;
	}
	public String getPopupText() {
		return popupText;
	}
	public void setPopupText(String popupText) {
		this.popupText = popupText;
	}

}
