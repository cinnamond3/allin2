package com.cafe24.allin2.controller;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

 



import com.cafe24.allin2.helpservice.WebHelpService;
import com.cafe24.allin2.model.PhoneCallModel;
import com.cafe24.allin2.service.PhoneCallService;
import com.sun.org.apache.xalan.internal.xsltc.compiler.Parser;

@Controller
@RequestMapping("/call")
public class PhoneCallController {
	// DI
	private ApplicationContext context = new GenericXmlApplicationContext("/config/applicationContext.xml");
	private PhoneCallService service = (PhoneCallService) context.getBean("phoneCallService");
	private WebHelpService webHelpService = new WebHelpService();
	//
	// * User variable
	// article, page variables
	private int currentPage = 1;
	private int showPhoneCallLimit = 10; // change value if want to show more articles by one page
	private int showPageLimit = 10; // change value if want to show more page links
	private int startPhoneCallNum = 0;
	private int endPhoneCallNum = 0;
	private int totalNum = 0;
	 
	
	@RequestMapping(value = "/mobile/callRegist.do", method=RequestMethod.POST)
	//public ModelAndView phoneCallRegist(@ModelAttribute("PhoneCallModel") PhoneCallModel phoneCallModel, MultipartHttpServletRequest request){
	public ModelAndView phoneCallRegist(HttpServletRequest request, HttpServletResponse response	){
			
		ModelAndView mav = new ModelAndView();
		PhoneCallModel phoneCallModel = new PhoneCallModel();
		
		String shop_code="";
		String debug="";
		String phone=""; 
		String banner_flg="";
		try
		{
			shop_code = request.getParameter("shop_code") ;
			 
			int totalNum = service.getShopCallNum(shop_code);
			
			phone = request.getParameter("phone") ;
			banner_flg = request.getParameter("banner_flg") ;
			
			if ( totalNum == 0 )
			{
				 
				service.insertShopCallCount(shop_code);
			}
			else
			{
				 
				service.updateShopCallCount(shop_code);
			}
			
			 
			phoneCallModel.setShop_code(shop_code);
			phoneCallModel.setPhone(phone);
			phoneCallModel.setBanner_flg(banner_flg);
			 
			
			service.moblieRegistPhoneCall(phoneCallModel);
			 
			 
			mav.addObject("result", "OK");
			mav.setViewName("/call/mobile/callRegist");
			return mav;
		}
		catch (Exception e) {
			
			mav.addObject("result", e.getMessage());
			 
			mav.setViewName("/call/mobile/callRegist");
			return mav;
		}
	}
	
	@RequestMapping("/list.do")
	public ModelAndView PhoneCallList(HttpServletRequest request, HttpServletResponse response){
		
		String startDate = null;
		String endDate=null;
		
		// set variables from request parameter
		if(request.getParameter("page") == null || request.getParameter("page").trim().isEmpty() || request.getParameter("page").equals("0")) {
			currentPage = 1;
		} else {
			currentPage = Integer.parseInt(request.getParameter("page"));
		}
		Map<String,String> searchMap = webHelpService.getSearchMap(request);
		
		if(request.getParameter("startDate") != null){
			startDate = request.getParameter("startDate").trim();
		}
		else
		{
			startDate = "1970-12-31 00:00:00";
		}
		
		if(request.getParameter("endDate") != null)
		{
			endDate = request.getParameter("endDate").trim();
		}
		else
		{
			endDate = "2099-12-31 00:00:00";
		}
		//
		
		// expression article variables value
		startPhoneCallNum = (currentPage - 1) * showPhoneCallLimit + 1;
		endPhoneCallNum = startPhoneCallNum + showPhoneCallLimit -1;
		//
		
		// get phoneCallList and get page html code
		WebHelpService whs = new WebHelpService();
		List<PhoneCallModel> phoneCallList;
		if(whs.isSearch(searchMap)){
			phoneCallList = service.searchPhoneCall(searchMap, startDate, endDate,  startPhoneCallNum, endPhoneCallNum);
			totalNum = service.getSearchTotalNum(searchMap, startDate, endDate);
		} else {
			phoneCallList = service.getPhoneCallList(startPhoneCallNum, endPhoneCallNum);
			totalNum = service.getTotalNum();
		}
		StringBuffer pageHtml = webHelpService.getPageHtml(currentPage, totalNum, showPhoneCallLimit, showPageLimit, searchMap);
		//
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("phoneCallList", phoneCallList);
		mav.addObject("pageHtml", pageHtml);
		mav.setViewName("/call/list");
		
		return mav;
	}
	
	  
	
	@RequestMapping("/mobile/MobileGPSRegist.do")
	public ModelAndView MobileGPSRegist(HttpServletRequest request, HttpServletResponse response){
		
		String provider = "";
		String latitude = "";		
		
		String longitude = "";
		 
		
		// set variables from request parameter
		 
		if(request.getParameter("provider") != null){
			provider = request.getParameter("provider").trim();
		}
		
		if(request.getParameter("latitude") != null){
			latitude = request.getParameter("latitude").trim();
		}
		
		if(request.getParameter("longitude") != null){
			longitude = request.getParameter("longitude").trim();
		}
		 
		 
		service.mobileGPSRegist(provider,latitude,longitude);
		ModelAndView mav = new ModelAndView();
		mav.addObject("result", "OK");
		
		mav.setViewName("/call/mobile/MobileGPSRegist");
		
		return mav;
	}
	
	  
}
