package com.cafe24.allin2.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.cafe24.allin2.helpservice.WebHelpService;
import com.cafe24.allin2.model.CallCountHistoryModel;
import com.cafe24.allin2.service.CallCountHistoryService;

@Controller
@RequestMapping("/callCountHistory")
public class CallCountHistoryController {
	// DI
	private ApplicationContext context = new GenericXmlApplicationContext("/config/applicationContext.xml");
	private CallCountHistoryService service = (CallCountHistoryService) context.getBean("callCountHistoryService");
	
	
	private WebHelpService webHelpService = new WebHelpService();
	
	Logger logger = Logger.getLogger(CallCountHistoryController.class);
	//
	// * User variable
	// article, page variables
	private int currentPage = 1;
	private static final int showArticleLimit = 10; // change value if want to show more articles by one page
	private static final int showPageLimit = 10; // change value if want to show more page links
	private int startNum = 0;
	private int endNum = 0;
	private int totalNum = 0;
	//
	
	
	@RequestMapping("/list.do")
	public ModelAndView getList(HttpServletRequest request, HttpServletResponse response){
		
		Map<String,Object> data = getPageHtml(request, showArticleLimit, showPageLimit);
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("callCountHistoryList", data.get("callCountHistoryList"));
		mav.addObject("pageHtml", data.get("pageHtml"));
		mav.setViewName("/callCountHistory/list");
		
		return mav;
	}
	
	public Map<String,Object> getPageHtml(HttpServletRequest request, int showArticleLimit, int showPageLimit){
		
		// set variables from request parameter
		if(request.getParameter("page") == null || request.getParameter("page").trim().isEmpty() || request.getParameter("page").equals("0")) {
			currentPage = 1;
		} else {
			currentPage = Integer.parseInt(request.getParameter("page"));
		}
		
		// expression article variables value
		startNum = (currentPage - 1) * showArticleLimit + 1;
		endNum = startNum + showArticleLimit -1;
		//
		
		// get boardList and get page html code
		List<CallCountHistoryModel> callCountHistoryList;
		Map<String,String> searchMap = webHelpService.getSearchMap(request);
		searchMap.put("startDate", StringUtils.trimToEmpty(request.getParameter("startDate")));
		searchMap.put("endDate", StringUtils.trimToEmpty(request.getParameter("endDate")));
		callCountHistoryList = service.search(searchMap, startNum, endNum);
		totalNum = service.getSearchTotalNum(searchMap);
		
		StringBuffer pageHtml = webHelpService.getPageHtml(currentPage, totalNum, showArticleLimit, showPageLimit, searchMap);
		
		Map<String,Object> data = new HashMap<String,Object>();
		data.put("callCountHistoryList", callCountHistoryList);
		data.put("pageHtml", pageHtml);
		
		return data;
	}
}
