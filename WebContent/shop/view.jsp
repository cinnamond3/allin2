<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>글 보기: ${board.subject}</title>
<link href="<%=request.getContextPath()%>/css/shop.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	function errCodeCheck(){
		var errCode = <%=request.getParameter("errCode")%>;
		if(errCode != null || errCode != ""){
			switch (errCode) {
			case 1:
				alert("잘못된 접근 경로입니다!");
				break;
			case 2:
				alert("댓글이 있어 글을 삭제하실 수 없습니다!");
				break;
			}
		}		
	}
	
	function moveAction(where){
		switch (where) {
		case 1:
			if(confirm("글을 삭제하시겠습니까?")){
				location.href ="delete.do?shop_code=${shop.shopCode}";
			}
			break;

		case 2:
			if(confirm("글을 수정하시겠습니까?")){
				location.href = "modify.do?shop_code=${shop.shopCode}";
			}
			break;
			
		case 3:
			location.href = "list.do";			
			break;
		
		case 4:
			if(confirm("글을 복사하시겠습니까?")){
				location.href = "modify.do?shop_code=${shop.shopCode}&isCopy=Y";
			}
			break;
		}
	}
	
	function viewImage(image) {

	    var popup_div = document.getElementById('popup_div');

	    //tempX = event.clientX + document.body.scrollLeft ;
	    //tempY = event.clientY + document.body.scrollTop ;
	    tempX = 200;
	    tempY = 200;
        
	    popup_div.style.left = tempX+'px';
	    popup_div.style.top  = tempY+'px';
	    
	    popup_div.style.position="absolute";
	    
	    popup_div.innerHTML="<div><img src='"+image.src+"'></div>";
	    popup_div.style.display='block';
		
	}
	
	function closeImage() {
	    var popup_div = document.getElementById('popup_div');
	    popup_div.style.display="none";
	    
	}
</script>
</head>
<body onload="errCodeCheck()">
<div class="wrapper">	
    <div class="nav">
        <jsp:include page="/common/menu.jsp"></jsp:include>
    </div>
    
    <h3>업체 조회</h3>
	<table class="view">
	   <colgroup>
	       <col width="15%">
	       <col width="35%">
	       <col width="15%">
	       <col width="35%">
	   </colgroup>
		<tr>
			<th>지역</th>
			<td >
                <c:forEach var="areaItem" items="${areaList}">
                    <c:if test="${areaItem.code_value == shop.areaCode}">${areaItem.code_desc}</c:if>
                </c:forEach>
            </td>
			<th>카테고리</th>
            <td >
                <c:forEach var="categoryItem" items="${categoryList}">
                    <c:if test="${categoryItem.code_value == shop.category}">${categoryItem.code_desc}</c:if>
                </c:forEach>
            </td>
		</tr>
		<tr>
			<th>업체코드</th><td>${shop.shopCode}</td>
			<th>업체명</th><td>${shop.shopName}</td>
		</tr>
		<tr>
			<th>전체주소</th><td class="" colspan="3">${shop.addressHeader}</td>
		</tr>
		<tr>
			<th>간단주소</th><td>${shop.addressTail}</td>
			<th> </th><td> </td>
		</tr>
		<tr>
			<th>전화번호</th><td>${shop.phone}</td>
			<th>실제전화번호</th><td>${shop.originalPhone}</td>
		</tr>
		<tr>
		    <th></th>
		    <td colspan="3" class="left"> * Mobile App에서는 전화번호로 전화 걸기가 연결됩니다.</td>
		</tr>
		<tr>
			<th>배달지역</th><td>${shop.deliverlyArea}</td>
			<th>영업시간</th><td>${shop.shopHours}</td>	
		</tr>
		<tr>
			<th>카드여부</th><td>${shop.creditYn}</td>		
			<th>결재수단</th><td>${shop.shopPay}</td>		
		</tr>
		<tr>
			<th>배달여부</th><td>${shop.deliverlyYn}</td>
			<th>메뉴여부</th><td>${shop.menuYn}</td>
		</tr>
		<tr>
			<th>쿠폰여부</th><td>${shop.couponYn}</td>
			<th> </th><td> </td>
		</tr>
		<tr>
			<th>업체설명</th><td>${shop.shopDesc}</td>	
			<th>휴무일</th><td>${shop.offDays}</td>
		</tr>
		<tr>
			<th>메뉴 Image 파일명</th>
			<td>	
			<c:if test="${shop.menuImgFileName != null }">
				<span class="gray">
				    ${shop.menuImgFileName}
				    <br/>
				    <img class="imgFile" src="<%=request.getContextPath()%>/shop/upload/${shop.menuImgFileName}" onclick="viewImage(this)"/>
				</span>
			</c:if>
			</td>
			<th>Small Log Image 파일명</th>
			<td>	
                <c:if test="${shop.smallLogImgFileName != null }">
                    <span class="gray">
                    ${shop.smallLogImgFileName}
                    <br/>
                    <img class="imgFile" src="<%=request.getContextPath()%>/shop/upload/${shop.smallLogImgFileName}" onclick="viewImage(this)"/>
                </span>
                </c:if>
			</td>
		</tr>
		<tr>
			<th>위도</th><td>${shop.latitude}</td>	
			<th>경도</th><td>${shop.longitude}</td>		
		</tr>
		<tr>
			<th>사용유무</th><td>${shop.useYn}</td>
            <th>업체등급</th>
            <td >
                <c:forEach var="shopGradeItem" items="${shopGradeList}">
                    <c:if test="${shopGradeItem.code_value == shop.shopGrade}">${shopGradeItem.code_desc}</c:if>
                </c:forEach>
            </td>
		</tr>
		<tr>
			<th>작성시간</th><td>${shop.writedate}</td>	
			<th>작성자</th><td>${shop.writer}</td>	
		</tr>
	</table>
	<br />
	<c:choose>
		<c:when test="${shop.writerid == userId}">
			<input type="button" value="삭제" class="writeBt" onclick="moveAction(1)" />
			<input type="button" value="수정" class="writeBt" onclick="moveAction(2)" />
			<input type="button" value="복사" class="writeBt" onclick="moveAction(4)" />
			<input type="button" value="목록" class="writeBt" onclick="moveAction(3)" />
		</c:when>
		<c:otherwise>
		    <input type="button" value="수정" class="writeBt" onclick="moveAction(2)" />
		    <input type="button" value="복사" class="writeBt" onclick="moveAction(4)" />
			<input type="button" value="목록" class="writeBt" onclick="moveAction(3)" />
		</c:otherwise>
	</c:choose>
</div>
<div id="popup_div" name="popup_div" onclick="closeImage(this)"></div> 
</body>
</html>