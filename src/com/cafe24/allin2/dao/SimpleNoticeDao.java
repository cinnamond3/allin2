package com.cafe24.allin2.dao;

import java.util.List;

import com.cafe24.allin2.model.SimpleNoticeModel;

public interface SimpleNoticeDao {

	SimpleNoticeModel getOne(String areaCode);
	
	boolean modify(SimpleNoticeModel simpleNotice);

	List<SimpleNoticeModel> getList();
	
}
