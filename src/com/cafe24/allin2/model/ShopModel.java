package com.cafe24.allin2.model;

public class ShopModel {
	private String shopCode;
	private String shopName;
	private String areaCode;
	private String areaName;
	private String category;
	private String categoryName;

	private String phone;
	private String originalPhone;
	private String addressHeader;
	private String addressTail;
	private String creditYn;
	private String shopPay;
	private String deliverlyYn;
	private String menuYn;
	private String couponYn;
	private String deliverlyArea;
	private String shopHours;
	private String offDays;
	private String menuImgFileName;
	private String smallLogImgFileName;
	private String shopDesc;
	private String latitude;
	private String longitude;
	private String useYn;
	private String shopGrade;
	private String writedate;
	private String writerid;
	private String writer;
	
	
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public void setWritedate(String writedate) {
		this.writedate = writedate;
	}
	public String getWritedate() {
		return writedate;
	}
	public String getWriterid() {
		return writerid;
	}
	public void setWriterid(String writerid) {
		this.writerid = writerid;
	}
	public String getWriter() {
		return writer;
	}
	public void setWriter(String writer) {
		this.writer = writer;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getOriginalPhone() {
		return originalPhone;
	}
	public void setOriginalPhone(String originalPhone) {
		this.originalPhone = originalPhone;
	}
	public String getAddressHeader() {
		return addressHeader;
	}
	public void setAddressHeader(String addressHeader) {
		this.addressHeader = addressHeader;
	}
	public String getAddressTail() {
		return addressTail;
	}
	public void setAddressTail(String addressTail) {
		this.addressTail = addressTail;
	}
	public String getCreditYn() {
		return creditYn;
	}
	public void setCreditYn(String creditYn) {
		this.creditYn = creditYn;
	}
	public String getShopPay() {
		return shopPay;
	}
	public void setShopPay(String shopPay) {
		this.shopPay = shopPay;
	}
	public String getDeliverlyYn() {
		return deliverlyYn;
	}
	public void setDeliverlyYn(String deliverlyYn) {
		this.deliverlyYn = deliverlyYn;
	}
	public String getMenuYn() {
		return menuYn;
	}
	public void setMenuYn(String menuYn) {
		this.menuYn = menuYn;
	}
	public String getCouponYn() {
		return couponYn;
	}
	public void setCouponYn(String couponYn) {
		this.couponYn = couponYn;
	}
	public String getDeliverlyArea() {
		return deliverlyArea;
	}
	public void setDeliverlyArea(String deliverlyArea) {
		this.deliverlyArea = deliverlyArea;
	}
	public String getShopHours() {
		return shopHours;
	}
	public void setShopHours(String shopHours) {
		this.shopHours = shopHours;
	}
	public String getOffDays() {
		return offDays;
	}
	public void setOffDays(String offDays) {
		this.offDays = offDays;
	}
	public String getMenuImgFileName() {
		return menuImgFileName;
	}
	public void setMenuImgFileName(String menuImgFileName) {
		this.menuImgFileName = menuImgFileName;
	}
	public String getSmallLogImgFileName() {
		return smallLogImgFileName;
	}
	public void setSmallLogImgFileName(String smallLogImgFileName) {
		this.smallLogImgFileName = smallLogImgFileName;
	}
	public String getShopDesc() {
		return shopDesc;
	}
	public void setShopDesc(String shopDesc) {
		this.shopDesc = shopDesc;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public String getShopGrade() {
		return shopGrade;
	}
	public void setShopGrade(String shopGrade) {
		this.shopGrade = shopGrade;
	}

	public String toString() {
		StringBuffer sbString = new StringBuffer();
		sbString.append("{shopCode=").append(shopCode).append(", ");
		sbString.append("shopName=").append(shopName).append(", ");
		sbString.append("phone=").append(phone).append(", ");
		sbString.append("addressHeader=").append(addressHeader).append(", ");
		sbString.append("addressTail=").append(addressTail).append(", ");
		sbString.append("creditYn=").append(creditYn).append(", ");
		sbString.append("shopPay=").append(shopPay).append(", ");
		sbString.append("deliverlyYn=").append(deliverlyYn).append(", ");
		sbString.append("menuYn=").append(menuYn).append(", ");
		sbString.append("couponYn=").append(couponYn).append(", ");
		sbString.append("deliverlyArea=").append(deliverlyArea).append(", ");
		sbString.append("shopHours=").append(shopHours).append(", ");
		sbString.append("offDays=").append(offDays).append(", ");
		sbString.append("menuImgFileName=").append(menuImgFileName).append(", ");
		sbString.append("smallLogImgFileName=").append(smallLogImgFileName).append(", ");
		sbString.append("shopDesc=").append(shopDesc).append(", ");
		sbString.append("latitude=").append(latitude).append(", ");
		sbString.append("longitude=").append(longitude).append(", ");
		sbString.append("useYn=").append(useYn).append(", ");
		sbString.append("shopGrade=").append(shopGrade).append("}");
		return sbString.toString();
	}
}
