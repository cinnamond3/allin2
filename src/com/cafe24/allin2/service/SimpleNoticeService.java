package com.cafe24.allin2.service;

import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.cafe24.allin2.dao.SimpleNoticeDao;
import com.cafe24.allin2.model.CodeModel;
import com.cafe24.allin2.model.SimpleNoticeModel;

public class SimpleNoticeService extends SqlMapClientDaoSupport implements SimpleNoticeDao {
	
	@Override
	public SimpleNoticeModel getOne(String areaCode) {
		return (SimpleNoticeModel) getSqlMapClientTemplate().queryForObject("simpleNotice.getOne", areaCode);
	}

	@Override
	public boolean modify(SimpleNoticeModel simpleNotice) {
		getSqlMapClientTemplate().update("simpleNotice.modify", simpleNotice);
		return true;
	}

	@Override
	public List<SimpleNoticeModel> getList() {
		
		return getSqlMapClientTemplate().queryForList("simpleNotice.getList");
	}	
}
