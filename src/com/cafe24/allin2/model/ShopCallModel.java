package com.cafe24.allin2.model;

public class ShopCallModel {

	private String shopCode;
	private String shopName;
	private String callMonth;
	private String callCount;
	private String searchCount;
	private String bannerCount;
	
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getCallMonth() {
		return callMonth;
	}
	public void setCallMonth(String callMonth) {
		this.callMonth = callMonth;
	}
	public String getCallCount() {
		return callCount;
	}
	public void setCallCount(String callCount) {
		this.callCount = callCount;
	}
	public String getSearchCount() {
		return searchCount;
	}
	public void setSearchCount(String searchCount) {
		this.searchCount = searchCount;
	}
	public String getBannerCount() {
		return bannerCount;
	}
	public void setBannerCount(String bannerCount) {
		this.bannerCount = bannerCount;
	}
	
	
}
