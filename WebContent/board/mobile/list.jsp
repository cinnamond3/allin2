<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 
<meta http-equiv="X-UA-Compatible" content="IE=9">


<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0">

<title>게시판 글 목록 보기</title>
<link href="<%=request.getContextPath()%>/css/board.css" rel="stylesheet" type="text/css" />
<link href="<%=request.getContextPath()%>/css/Style-ETC.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript">
<!--
	function selectedOptionCheck(){
		$("#type > option[value=<%=request.getParameter("type")%>]").attr("selected", "true");
	}
	
	function moveAction(where){
		switch (where) {
		case 1:
			location.href = "write.do";
			break;
		
		case 2:
			location.href = "list.do";
			break;
		}
	}
//-->
	function formatDate(writedate)
		{
			var a_temp_cookie = '';
			a_temp_cookie=writedate.substr(0,10);
			return a_temp_cookie;
		}
</script>
</head>
<body onload="selectedOptionCheck()">
	
<div id="ct">
	
<div class="lst_section ">
	<ul class="lst4" id="noticeList" style="display:none">
		
	</ul>
		
		
	 <ul class="lst4">
		 	
		<c:forEach var="board" items="${boardList}">
		<li class="">
	   	
			 
				<c:if test="${board.comment >= 10}"><img src="<%=request.getContextPath()%>/img/hit.jpg" /></c:if>
				<a href="view.do?idx=${board.idx}">
					<p>
						<strong>${board.subject}</strong>      
					   	  
						<br />
						<span class="info">
							<span class="ty">${board.writer}</span>
							<span class="bar">|</span> ${board.writeDate}
							<span class="bar">|</span> 조회 ${board.hitcount}
						</span>
					</p>
					
				</a>
			 
					
		</li>
	
	  
		
		</c:forEach>
		
		 
	</ul>
</div> </div> 	 
	 
	<div class="wrapper">
	<br />
	
	${pageHtml}
	<br /><br />
		</div> 
	
</body>
</html>