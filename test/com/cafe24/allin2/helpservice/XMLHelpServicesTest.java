package com.cafe24.allin2.helpservice;

import static org.junit.Assert.*;

import java.io.IOException;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.w3c.dom.Document;

public class XMLHelpServicesTest {

	XMLHelpService service = new XMLHelpService();
	Logger logger = Logger.getLogger(XMLHelpServicesTest.class); 
	@Test
	public void testLoadXMLFromString() throws ServletException, IOException {
		StringBuffer sbXml = new StringBuffer("<geocode xmlns=\"naver:openapi\">");
		sbXml.append("<userquery><![CDATA[�뱸�����ô޼������ﵿ124-8����]]></userquery>");
		sbXml.append("<total>1</total>");
		sbXml.append("<item>");
		sbXml.append("<point>");
		sbXml.append("<x>449494</x>");
		sbXml.append("<y>361275</y>");
		sbXml.append("</point>");
		sbXml.append("<address>�뱸������ �޼��� ���ﵿ 124-8</address>");
		sbXml.append("<addrdetail>");
		sbXml.append("<sido><![CDATA[�뱸������]]>");
		sbXml.append("<sigugun><![CDATA[�޼���]]>");
		sbXml.append("<dongmyun><![CDATA[���ﵿ]]>");
		sbXml.append("<rest><![CDATA[124-8]]></rest>");
		sbXml.append("</dongmyun>");
		sbXml.append("</sigugun>");
		sbXml.append("</sido>");
		sbXml.append("</addrdetail>");
		sbXml.append("</item>");
		sbXml.append("</geocode>");
		Document doc = service.loadXMLFromString(sbXml.toString());

		logger.debug(doc.getElementsByTagName("x").item(0).getTextContent());
		
	}

}
