<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>글 보기: ${board.subject}</title>
<link href="<%=request.getContextPath()%>/css/banner.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	function errCodeCheck(){
		var errCode = <%=request.getParameter("errCode")%>;
		if(errCode != null || errCode != ""){
			switch (errCode) {
			case 1:
				alert("잘못된 접근 경로입니다!");
				break;
			}
		}		
	}
	
	function moveAction(where){
		switch (where) {
		case 1:
			if(confirm("글을 삭제하시겠습니까?")){
				location.href ="delete.do?bannerId=${banner.bannerId}";
			}
			break;

		case 2:
			if(confirm("글을 수정하시겠습니까?")){
				location.href = "modify.do?bannerId=${banner.bannerId}";
			}
			break;
			
		case 3:
			location.href = "list.do";			
			break;
		}
	}
	
    function viewImage(image) {

        var popup_div = document.getElementById('popup_div');

        //tempX = event.clientX + document.body.scrollLeft ;
        //tempY = event.clientY + document.body.scrollTop ;
        tempX = 200;
        tempY = 200;
        
        popup_div.style.left = tempX+'px';
        popup_div.style.top  = tempY+'px';
        
        popup_div.style.position="absolute";
        
        popup_div.innerHTML="<div><img src='"+image.src+"'></div>";
        popup_div.style.display='block';
        
    }
    
    function closeImage() {
        var popup_div = document.getElementById('popup_div');
        popup_div.style.display="none";
        
    }

</script>
</head>
<body onload="errCodeCheck()">
<div class="wrapper">	
    <div class="nav">
        <jsp:include page="/common/menu.jsp"></jsp:include>
    </div>
    
	<table class="view">
	   <colgroup>
	       <col width="20%">
	       <col width="80%">
	   </colgroup>
		<tr>
			<th>배너ID</th><td>${banner.bannerId}</td>
		</tr>
		<tr>
	        <th>지역</th>
	        <td >
	            <c:forEach var="areaItem" items="${areaList}">
	                <c:if test="${areaItem.code_value == banner.areaCode}">${areaItem.code_desc}</c:if>
	            </c:forEach>
	        </td>
        </tr>
        <tr>
	        <th>카테고리</th>
	        <td >
	            <c:forEach var="categoryItem" items="${categoryList}">
	                <c:if test="${categoryItem.code_value == banner.category}">${categoryItem.code_desc}</c:if>
	            </c:forEach>
            </td>
		</tr>
		

		<tr>
			<th>배너 Image</th>
			<td>    
            <c:if test="${banner.imgFileName != null }">
                <span class="gray">
                    ${banner.imgFileName}
                    <br/>
                    <img class="imgFile" src="<%=request.getContextPath()%>/banner/upload/${banner.imgFileName}" onclick="viewImage(this)"/>
                </span>
            </c:if>
            </td>
		</tr>
		<tr>
			<th>업체 코드</th>
			<td>${banner.shopCode}</td>	
		</tr>
		<tr>
			<th>웹 URL링크</th>
			<td>${banner.webUrl}</td>	
		</tr>
	</table>
	<br />
	<input type="button" value="삭제" class="writeBt" onclick="moveAction(1)" />
    <input type="button" value="수정" class="writeBt" onclick="moveAction(2)" />
	<input type="button" value="목록" class="writeBt" onclick="moveAction(3)" />
</div>
<div id="popup_div" name="popup_div" onclick="closeImage(this)"></div>
</body>
</html>