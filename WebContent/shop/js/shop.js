	function writeFormCheck() {
		if($("#shopName").val() == null || $("#shopName").val() == ""){
			alert("제목을 입력해 주세요!");
			$("#shopName").focus();
			return false;
		}
		
		return true;
	}
	
	function moveAction(where){
		switch (where) {
		case 1:
			location.href ="list.do";
			break;
		}
	}

    
    $(document).ready(function(){
        $("#getMapXY").click(function(){
            //data:{key:"4e39692c552db937b6e8e0ddd8148ead", encoding:"utf-8", coord:"tm128", query:"대구광역시달서구감삼동124-8번지"},
            //alert($("#addressHeader").val());
            $.ajax({
                type:"GET",
                url:"/shop/getAddress.do",
                dataType:"json",
                //data:"query=" + $("#addressHeader").val() + "&coord=latlng",
                data:{query:$("#addressHeader").val(), coord:"latlng"}, 
                error:function(request, status, error){
                    alert("code: " + request.status + "\r\nmessage: " + request.responseText + "\r\nerror: " + error);
                },
                success:function(response, status, request){
                	if (response == "error") {
                		alert("주소를 정확하게 입력하세요.");
                		$("#addressHeader").focus();
                	} else {
	                    alert(response);
	                	$("#latitude").val(response.split(",")[1]);
	                	$("#longitude").val(response.split(",")[0]);
                    }
                }
            });
        });
    });
