<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>글 보기: ${board.subject}</title>
<link href="<%=request.getContextPath()%>/css/board.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    
    function moveAction(where){
        switch (where) {
        
        case 1:
            location.href ="list.do";
            break;
        case 2:
            if(confirm("글을 수정하시겠습니까?")){
                location.href = "modify.do?areaCode=${simpleNotice.areaCode}";
            }
            break;
        }
    }
</script>
</head>
<body onload="">
<div class="wrapper">   
    <div class="nav">
        <jsp:include page="/common/menu.jsp"></jsp:include>
    </div>  
    <table class="boardView">
        <colgroup>
          <col width="20%">
          <col width="80%">
        </colgroup>
        <tr>
            <th>지역</th>
            <td>
                <c:forEach var="areaItem" items="${areaList}">
                    <c:if test="${areaItem.code_value == simpleNotice.areaCode}">${areaItem.code_desc}</c:if>
                </c:forEach>
            </td>
        </tr>
                
        <tr>
            <th>APP 메인 한줄공지</th><td>${simpleNotice.notifyText}</td>
        </tr>
        <tr>
            <th>APP 메인 한줄공지링크</th><td>${simpleNotice.notifyLink}</td>
        </tr>
        <tr>
            <th>APP 메인 팝업공지</th><td>${simpleNotice.popupText}</td>
        </tr>
    </table>    
    <br/>
    <input type="button" value="목록" class="writeBt" onclick="moveAction(1)" />    
    <input type="button" value="수정" class="writeBt" onclick="moveAction(2)" />
</div>
</body>
</html>