<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<link href="<%=request.getContextPath()%>/css/main.css" rel="stylesheet" type="text/css" />
<ul class="nav">
	<li class="nav"><a href="/login.do">로그인</a></li>
	<li class="nav"><a href="/member/join.do">회원가입</a></li>
	<li class="nav"><a href="/simpleNotice/list.do">알람관리</a></li>
	<li class="nav"><a href="/board/list.do">공지사항</a></li>
	<li class="nav"><a href="/shop/list.do">업체관리</a></li>
	<li class="nav"><a href="/banner/list.do">배너관리</a></li>
	<li class="nav"><a href="/mobileSync/list.do">푸쉬관리</a></li>
	<li class="nav"><a href="/callCountHistory/list.do">전화이력조회</a></li>
	<li class="nav"><a href="/shopCall/list.do">업체별 월실적 조회</a></li>
	<li class="nav"><a href="/logout.do">로그아웃</a></li>		
</ul>
<br/>

<!-- ul class="nav">
	<li class="nav">Moble 테스트 메뉴 목록 ==></li>
	<li class="nav"><a href="/simpleNotice/mobile/view.do">알람관리</a></li>
	<li class="nav"><a href="/board/mobile/list.do">공지사항</a></li>
	<li class="nav"><a href="/board/mobile/list.do?areaCode=AA&header=이벤트">공지사항(header조회)</a></li>
	<li class="nav"><a href="/shop/mobile/list.do?page=1">업체리스트조회</a></li>
	<li class="nav"><a href="/shop/mobile/view.do?shopCode=10181">업체상세조회</a></li>
	<li class="nav"><a href="/shop/mobile/driverList.do?type=area_code&keyword=AB&type1=category&keyword1=BA">대리운전 업체 조회</a></li>
	<li class="nav"><a href="/banner/mobile/list.do">배너관리</a></li>
	<li class="nav"><a href="/code/mobile/list.do">코드관리</a></li>
</ul-->
