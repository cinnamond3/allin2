package com.cafe24.allin2.model;

public class BannerModel {

	
	private String bannerId;
	private String areaCode;
	private String category;
	public String getCategory() {
		return category;
	}
	public void setCategory(String categoty) {
		this.category = categoty;
	}
	private String imgFileName;
	private String shopCode;
	private String webUrl;
	public String getBannerId() {
		return bannerId;
	}
	public void setBannerId(String bannerId) {
		this.bannerId = bannerId;
	}
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	public String getImgFileName() {
		return imgFileName;
	}
	public void setImgFileName(String imgFileName) {
		this.imgFileName = imgFileName;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getWebUrl() {
		return webUrl;
	}
	public void setWebUrl(String webUrl) {
		this.webUrl = webUrl;
	}
	@Override
	public String toString() {
		return "BannerModel [bannerId=" + bannerId + ", areaCode=" + areaCode
				+ ", category=" + category + ", imgFileName=" + imgFileName
				+ ", shopCode=" + shopCode + ", webUrl=" + webUrl + "]";
	}
	
	
}
