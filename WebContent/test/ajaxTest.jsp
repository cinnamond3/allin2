<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ajax 테스트</title>
<link href="<%=request.getContextPath()%>/css/shop.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript">
    
    $(document).ready(function(){
        $("#getMapXY").click(function(){
            //data:{key:"4e39692c552db937b6e8e0ddd8148ead", encoding:"utf-8", coord:"tm128", query:"대구광역시달서구감삼동124-8번지"},
            $.ajax({
                type:"GET",
                url:"/shop/getAddress.do",
                dataType:"json",
                data:"query=대구광역시달서구감삼동124-8번지",
                error:function(request, status, error){
                    alert("code: " + request.status + "\r\nmessage: " + request.responseText + "\r\nerror: " + error);
                },
                success:function(response, status, request){
                    alert("response: " + response);
                }
            });
        });
    });
    
</script>
</head>
<body>
<input type="button" id="getMapXY" value="테스트"/>
</body>
</html>