package com.cafe24.allin2.interceptor;

import static org.junit.Assert.*;

import org.junit.Test;

public class SessionInterceptorTest {

	@Test
	public void testCheckByPassURI() throws Exception {
		String requestURI = "/simpleNotice/mobile/view.do";
		
		SessionInterceptor test = new SessionInterceptor();
		boolean actual = test.checkByPassURI(requestURI);
		
		assertTrue(actual);
		//assertFalse(actual);
	}
}
