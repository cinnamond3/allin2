package com.cafe24.allin2.dao;

import java.util.List;








import java.util.Map;

import com.cafe24.allin2.model.CodeModel;
import com.cafe24.allin2.model.MobileUserModel;

public interface MobileUserDao {
	// get all contents in JMMobileUser table
	List<MobileUserModel> getMobileUserList(int startMobileUserNum, int showMobileUserLimit);
	 
	// get search result list
	List<MobileUserModel> searchMobileUser(Map<String,String> searchMap,  int startMobileUserNum, int endMobileUserNum); 
	
	boolean moblieRegistMobileUser(MobileUserModel mobileUser );
	    
	// get contents count
	int getTotalNum();
	
	// get count of search results
	int getSearchTotalNum(Map<String,String> searchMap );
	
    MobileUserModel getMobileUserOne(String phone);
    
    void updateMobileUser(String phone_num, String area_code, String reg_id, String use_yn );
	
    List<CodeModel> searchAreaCode(Map<String,String> searchMap);
}
