<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>푸시 대상 번호 보기</title>
<link href="<%=request.getContextPath()%>/css/board.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript">
		
	function writeFormCheck() {
		 
		return true;
	}
	
	
<!--
	function selectedOptionCheck(){
		$("#keyword > option[value=<%=request.getParameter("keyword")%>]").attr("selected", "true");
	}
	
	function moveAction(where){
		switch (where) {
		case 1:
			location.href = "list.do";
			break;
		
		}
	}
//-->
</script>
</head>
<body onload="selectedOptionCheck()">
<div class="wrapper">
	<div class="nav">
		<jsp:include page="/common/menu.jsp"></jsp:include>
	</div>
	<div class="header">
		<form action="list.do" method="get">	
			<input type="hidden" name="type" value="area_code">	
			<label for="userId"> 지역설정 : </label>
			
			<select id="keyword" name="keyword">
				<option value="">전지역</option>
			  <c:forEach var="code" items="${codeList}">
			
					<option value="${code.code_value}">
						${code.code_desc}</option>
				 
				</c:forEach>
					</select>
			<label for="phoneNum"> 전화번호 : </label>
		  <input type="hidden" name="type1" value="phone_number">
			<input type="text" id="keyword1" name="keyword1" value="<%if(request.getParameter("keyword1") != null){ out.print(request.getParameter("keyword1")); } else { out.print(""); }%>" />
			<input type="submit" value="검색" />			
		</form>
	   
	   
	   <form action="GCM_Send.do" method="post" onsubmit="return writeFormCheck()" enctype="multipart/form-data">	
	   	 <input type="hidden" id="area_code" name="area_code" value="<%if(request.getParameter("keyword") != null){ out.print(request.getParameter("keyword")); } else { out.print(""); }%>" />
			 <table class="boardWrite">	
			 	<tr>
					<th><label for="subject">타이틀(10자내외)</label></th>
					<td>
						<input type="text" id="subject" name="subject" class="boardSubject"/>
					</td>			
				</tr>
				
			 	 
		   <tr>
				<th><label for="content">내용(20자내외)</label></th>
				<td><textarea id="content" name="content" class="boardContent"></textarea></td>			
			</tr>
		</table>
		<br />
	 <input type="reset" value="재작성" class="writeBt"/>
	 <input type="submit" value="보내기" class="writeBt"/>
	  
	 </form>
	 <br>
	 <table>
	 	<tr>
	 		<th><label for="content">아래 대상 리스트에 한해 발송됩니다.</label> </th>
		</tr>
		</table>
	 
	<table border="0" class="boardTable">
		<thead>
		<tr>
			<th>순번</th>
			<th>지역</th>
			<th>핸드폰 번호</th>
			<th>PUSH 고유키</th>
			
		</tr>
		</thead>
		<tbody>
		<c:forEach var="mobileUser" items="${mobileUserList}">
		<tr>
			<td class="idx">${mobileUser.rnum}</td>
			<td class="idx">${mobileUser.area_code}</td>
			<td class="comment">${mobileUser.phone_number}</td>
			<td class="hitcount">${mobileUser.reg_id}</td>
			
		</tr>
		</c:forEach>
		</tbody>
	</table>
	<br />
	${pageHtml}
	<br /><br />
	<input type="button" value="목록" class="writeBt" onclick="moveAction(1)"/>
	
</div>
</body>
</html>