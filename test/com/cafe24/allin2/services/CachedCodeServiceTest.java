package com.cafe24.allin2.services;

import static org.junit.Assert.*;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

import com.cafe24.allin2.model.CodeModel;
import com.cafe24.allin2.service.CachedCodeService;
import com.cafe24.allin2.service.CodeService;
import com.cafe24.allin2.service.ShopService;

public class CachedCodeServiceTest {

	private static CachedCodeService service;

	static Logger logger = Logger.getLogger(ShopServiceTest.class);
			
	@BeforeClass
	public static void setUpBeforeClass() {
		service = new CachedCodeService();
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetCodeList() {
		//given
		String codeName = "AREA";

		//when
		List<CodeModel> codes = service.getCodeList(codeName);
		
		
		assertEquals("[CodeModel [code_name=null, code_value=AA, code_desc=배방, writeDate=null], CodeModel [code_name=null, code_value=AB, code_desc=아산, writeDate=null]]", codes.toString());
	}
	
	@Test
	public void testGetCodeList2() {
		//given
		String codeName = "AREA";
		
		//when
		List<CodeModel> codes = service.getCodeList(codeName);
		
		
		assertEquals("[CodeModel [code_name=null, code_value=AA, code_desc=배방, writeDate=null], CodeModel [code_name=null, code_value=AB, code_desc=아산, writeDate=null]]", codes.toString());
	}

}
