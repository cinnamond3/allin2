package com.cafe24.allin2.services;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

import com.cafe24.allin2.model.ShopModel;
import com.cafe24.allin2.service.BoardService;
import com.cafe24.allin2.service.ShopService;

public class ShopServiceTest {

	private static ShopService service;

	static Logger logger = Logger.getLogger(ShopServiceTest.class);
			
	@BeforeClass
	public static void setUpBeforeClass() {
		ApplicationContext context = new GenericXmlApplicationContext("/config/applicationContext.xml");
		service = (ShopService) context.getBean("shopService");
	}
	
	@Test
	public void testGetOne() {

		ShopModel shop = service.getOne("10181");
		System.out.println("shop > " + shop);
	}

	
	@Test
	public void testSearch() {
		
		Map<String,String> searchMap = new HashMap<String,String>();
		searchMap.put("type", "area_code");
		searchMap.put("keyword", "AA");
		searchMap.put("type1", "category");
		searchMap.put("keyword1", "BA");
		List<ShopModel> shopList = service.search(searchMap);
		System.out.println("shopList > " + shopList);
	}
	
	@Test
	public void testModify() {
		
		ShopModel shop = new ShopModel();
		shop.setShopCode("10181");
		shop.setShopName("41456");
		shop.setAreaCode("AA");
		shop.setCategory("BA");
		shop.setAddressHeader("전체주소");
		shop.setAddressTail("간단주소");
		shop.setPhone("415682256");
		shop.setOriginalPhone("415682256");
		shop.setDeliverlyArea("배방 전지역");
		shop.setShopHours("오후4시~새벽17시");
		shop.setCreditYn("Y");
		shop.setShopPay("카드결제 가능");
		shop.setDeliverlyYn("Y");
		shop.setMenuYn("Y");
		shop.setCouponYn("Y");
		shop.setShopDesc("항상 최고의 음식을 제공합니다. 많이 이용바랍니다.");
		shop.setOffDays("연중무휴");
		shop.setMenuImgFileName("/shop/upload/10003_m.gif");
		shop.setSmallLogImgFileName("/shop/upload/10001_s.gif");
		shop.setLatitude("36.770938");
		shop.setLongitude("127.065457");
		shop.setUseYn("Y");
		shop.setShopGrade("1");
		shop.setWriterid("1");
		
		service.modify(shop);
		System.out.println("shop > " + shop);
	}
	
	@Test
	public void testWrite() {
		
		ShopModel shop = new ShopModel();
		shop.setShopCode("10183");
		shop.setShopName("41456");
		shop.setAreaCode("AA");
		shop.setCategory("BA");
		shop.setAddressHeader("전체주소");
		shop.setAddressTail("간단주소");
		shop.setPhone("415682256");
		shop.setOriginalPhone("415682256");
		shop.setDeliverlyArea("배방 전지역");
		shop.setShopHours("오후4시~새벽17시");
		shop.setCreditYn("Y");
		shop.setShopPay("카드결제 가능");
		shop.setDeliverlyYn("Y");
		shop.setMenuYn("Y");
		shop.setCouponYn("Y");
		shop.setShopDesc("항상 최고의 음식을 제공합니다. 많이 이용바랍니다.");
		shop.setOffDays("연중무휴");
		shop.setMenuImgFileName("/shop/upload/10003_m.gif");
		shop.setSmallLogImgFileName("/shop/upload/10001_s.gif");
		shop.setLatitude("36.770938");
		shop.setLongitude("127.065457");
		shop.setUseYn("Y");
		shop.setShopGrade("1");
		shop.setWriterid("1");
		
		service.write(shop);
		System.out.println("shop > " + shop);
	}
	
	@Test
	public void testGetNewShopCode() {
		
		String shopCode = service.getNewShopCode();
		System.out.println("shopCode > " + shopCode);
	}

}
