package com.cafe24.allin2.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.cafe24.allin2.helpservice.HttpHelpService;
import com.cafe24.allin2.helpservice.WebHelpService;
import com.cafe24.allin2.helpservice.XMLHelpService;
import com.cafe24.allin2.model.CodeModel;
import com.cafe24.allin2.model.DriverModel;
import com.cafe24.allin2.model.ShopModel;
import com.cafe24.allin2.model.SimpleShopModel;
import com.cafe24.allin2.model.SimpleShopModelConveter;
import com.cafe24.allin2.service.CachedCodeService;
import com.cafe24.allin2.service.ShopService;

@Controller
@RequestMapping("/shop")
public class ShopController {
	private static final String SHOP_UPLOAD_PATH = "shop/upload/";
	// DI
	private ApplicationContext context = new GenericXmlApplicationContext("/config/applicationContext.xml");
	private ShopService service = (ShopService) context.getBean("shopService");
	private WebHelpService webHelpService = new WebHelpService();
	private XMLHelpService xmlHelpService = new XMLHelpService();
	private HttpHelpService httpHelpService = new HttpHelpService();
	private CachedCodeService cachedCodeService = new CachedCodeService();
	//
	// * User variable
	// article, page variables
	private int currentPage = 1;
	private static final int showArticleLimit = 10; // change value if want to show more articles by one page
	private static final int showPageLimit = 10; // change value if want to show more page links
	private int startArticleNum = 0;
	private int endArticleNum = 0;
	private int totalNum = 0;
	
	
	//
	static Logger logger = Logger.getLogger(ShopController.class); 
	
	
	@RequestMapping("/list.do")
	public ModelAndView getList(HttpServletRequest request, HttpServletResponse response){
		
		Map<String,Object> data = getPageHtml(request, showArticleLimit, showPageLimit);
		ModelAndView mav = new ModelAndView();
		
		List<CodeModel> areaList = cachedCodeService.getCodeList("AREA");
		List<CodeModel> categoryList = cachedCodeService.getCodeList("CATEGORY");
		List<CodeModel> shopGradeList = cachedCodeService.getCodeList("SHOP_GRADE");
		
		mav.addObject("shopList", data.get("shopList"));
		mav.addObject("pageHtml", data.get("pageHtml"));
		mav.addObject("areaList", areaList);
		mav.addObject("categoryList", categoryList);
		mav.addObject("shopGradeList", shopGradeList);
		mav.setViewName("/shop/list");
		
		return mav;
	}
	
	@RequestMapping("/mobile/list.do")
	@ResponseBody
	public List<SimpleShopModel> getJsonList(HttpServletRequest request, HttpServletResponse response){
		
		Map<String,String> searchMap = webHelpService.getSearchMap(request);
		List<ShopModel> shopList = service.search(searchMap);
		
		List<SimpleShopModel> simpleShopList = new SimpleShopModelConveter().covertList(shopList);
		
		for (SimpleShopModel simpleShop : simpleShopList) {
			String smallLogFileName = simpleShop.getSmallLogImgFileName();
			simpleShop.setSmallLogImgFileName(webHelpService.getUploadFilePath(request, SHOP_UPLOAD_PATH) + smallLogFileName);
		}
		
		
		return simpleShopList;
	}
	
	@RequestMapping("/mobile/driverList.do")
	@ResponseBody
	public List<DriverModel> getDriverJsonList(HttpServletRequest request, HttpServletResponse response){
		
		Map<String,String> searchMap = webHelpService.getSearchMap(request);
		List<DriverModel> driverList = service.searchDriver(searchMap);
		
		for (DriverModel driver : driverList) {
			String menuImgFileName = driver.getMenuImgFileName();
			String smallLogFileName = driver.getSmallLogImgFileName();
			
			driver.setMenuImgFileName(webHelpService.getUploadFilePath(request, SHOP_UPLOAD_PATH) + menuImgFileName);
			driver.setSmallLogImgFileName(webHelpService.getUploadFilePath(request, SHOP_UPLOAD_PATH) + smallLogFileName);
			
		}
		
		
		return driverList;
	}


	
	public Map<String,Object> getPageHtml(HttpServletRequest request, int showArticleLimit, int showPageLimit){
		
		// set variables from request parameter
		if(request.getParameter("page") == null || request.getParameter("page").trim().isEmpty() || request.getParameter("page").equals("0")) {
			currentPage = 1;
		} else {
			currentPage = Integer.parseInt(request.getParameter("page"));
		}
		
		Map<String,String> searchMap = webHelpService.getSearchMap(request);
		//
		
		// expression article variables value
		startArticleNum = (currentPage - 1) * showArticleLimit + 1;
		endArticleNum = startArticleNum + showArticleLimit -1;
		//
		
		// get shopList and get page html code
		List<ShopModel> shopList;
		if(webHelpService.isSearch(searchMap)){
			shopList = service.searchForPage(searchMap, startArticleNum, endArticleNum);
			totalNum = service.getSearchTotalNum(searchMap);
		} else {
			shopList = service.getShopList(startArticleNum, endArticleNum);
			totalNum = service.getTotalNum();
		}
		StringBuffer pageHtml = webHelpService.getPageHtml(currentPage, totalNum, showArticleLimit, showPageLimit, searchMap);
		
		Map<String,Object> data = new HashMap<String,Object>();
		data.put("shopList", shopList);
		data.put("pageHtml", pageHtml);
		
		return data;
	}
	

	
	
	@RequestMapping("/view.do")
	public ModelAndView shopView(HttpServletRequest request){
		String shopCode = request.getParameter("shopCode");
		ShopModel shop = service.getOne(shopCode); // get selected article model
		
		List<CodeModel> areaList = cachedCodeService.getCodeList("AREA");
		List<CodeModel> categoryList = cachedCodeService.getCodeList("CATEGORY");
		List<CodeModel> shopGradeList = cachedCodeService.getCodeList("SHOP_GRADE");
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("shop", shop);
		mav.addObject("areaList", areaList);
		mav.addObject("categoryList", categoryList);
		mav.addObject("shopGradeList", shopGradeList);
		mav.setViewName("/shop/view");
		return mav;
	}
	
	@RequestMapping("/mobile/view.do")
	@ResponseBody
	public ShopModel shopJsonView(HttpServletRequest request){
		String shopCode = request.getParameter("shopCode");
		String bannerFlg = StringUtils.trimToEmpty(request.getParameter("bannerFlg"));
		ShopModel shop = service.getOne(shopCode); // get selected article model
		
		String menuImgFileName = shop.getMenuImgFileName();
		String smallLogFileName = shop.getSmallLogImgFileName();
		
		
		shop.setMenuImgFileName(webHelpService.getUploadFilePath(request, SHOP_UPLOAD_PATH) + menuImgFileName);
		shop.setSmallLogImgFileName(webHelpService.getUploadFilePath(request, SHOP_UPLOAD_PATH) + smallLogFileName);
		
		
		if (bannerFlg.equalsIgnoreCase("y")) {
			service.updateBannerCount(shopCode); // update hitcount
		} else {
			service.updateSearchCount(shopCode); // update hitcount
		}
		
		return shop;
	}

	
	@RequestMapping("/write.do")
	public ModelAndView shopWrite(@ModelAttribute("ShopModel") ShopModel shopModel){
		
		List<CodeModel> areaList = cachedCodeService.getCodeList("AREA");
		List<CodeModel> categoryList = cachedCodeService.getCodeList("CATEGORY");
		List<CodeModel> shopGradeList = cachedCodeService.getCodeList("SHOP_GRADE");
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("areaList", areaList);
		mav.addObject("categoryList", categoryList);
		mav.addObject("shopGradeList", shopGradeList);
		mav.setViewName("/shop/write");
		
		return mav;
	}
	
	@RequestMapping(value="/write.do", method=RequestMethod.POST)
	public String shopWriteProc(@ModelAttribute("ShopModel") ShopModel shopModel, MultipartHttpServletRequest request){
		
		MultipartFile newMenuImgFile = request.getFile("newMenuImgFile");
		String newMenuImgFileName = newMenuImgFile.getOriginalFilename();
		
		if(newMenuImgFile != null && !newMenuImgFileName.equals("")){
			webHelpService.createNewUploadFile(request, SHOP_UPLOAD_PATH, newMenuImgFile, newMenuImgFileName);
			shopModel.setMenuImgFileName(newMenuImgFileName);
		}
		
		
		MultipartFile newsmallLogImgFile = request.getFile("newSmallLogImgFile");
		String newSmallLogImgFileName = newsmallLogImgFile.getOriginalFilename();
		
		if(newsmallLogImgFile != null && !newSmallLogImgFileName.equals("")){
			webHelpService.createNewUploadFile(request, SHOP_UPLOAD_PATH, newsmallLogImgFile, newSmallLogImgFileName);
			shopModel.setSmallLogImgFileName(newSmallLogImgFileName);
		} 
		//
		// new line code change to <br /> tag	
		//String content =  shopModel.getContent().replaceAll("\r\n", "<br />");		
		//shopModel.setContent(content);
		//
		shopModel.setShopCode(service.getNewShopCode());
		logger.debug(shopModel);
		service.write(shopModel);		
		
		return "redirect:list.do";
	}

	@RequestMapping("/modify.do")
	public ModelAndView shopModify(HttpServletRequest request, HttpSession session){
		String userId = (String) session.getAttribute("userId");
		String shopCode = request.getParameter("shop_code");
		String isCopy = StringUtils.trimToEmpty(request.getParameter("isCopy"));
		
		ShopModel shop = service.getOne(shopCode);
		
		if (isCopy.equals("Y")) {
			shop.setShopCode(service.getNewShopCode());
		}
		// <br /> tag change to new line code
		//String content = shop.getContent().replaceAll("<br />", "\r\n");
		//shop.setContent(content);
		//
		
		List<CodeModel> areaList = cachedCodeService.getCodeList("AREA");
		List<CodeModel> categoryList = cachedCodeService.getCodeList("CATEGORY");
		List<CodeModel> shopGradeList = cachedCodeService.getCodeList("SHOP_GRADE");
		
		ModelAndView mav = new ModelAndView();
		
		if(userId == null){ 
			//mav.addObject("shop", shop);
			//mav.setViewName("/shop/modify");
			
			mav.addObject("errCode", "0");	// 로긴 하지 않고 접근
			mav.setViewName("redirect:/login.do");
		} else if(!userId.equals(shop.getWriterid())){
			mav.addObject("errCode", "1");	// forbidden connection
			mav.addObject("shopCode", shopCode);
			mav.setViewName("redirect:view.do");
		} else {
			mav.addObject("isCopy", isCopy);
			mav.addObject("shop", shop);
			mav.addObject("areaList", areaList);
			mav.addObject("categoryList", categoryList);
			mav.addObject("shopGradeList", shopGradeList);
			mav.setViewName("/shop/modify");
		}		
		return mav;
	}
	
	@RequestMapping(value = "/modify.do", method=RequestMethod.POST)
	public ModelAndView shopModifyProc(@ModelAttribute("ShopModel") ShopModel shopModel, MultipartHttpServletRequest request){
		
		String menuImgFileName = request.getParameter("menuImgFileName");
		MultipartFile newMenuImgFile = request.getFile("newMenuImgFile");
		String newMenuImgFileName = newMenuImgFile.getOriginalFilename();
		String menuImgFileDeleteFlag = request.getParameter("menuImgFileDeleteFlag");
		String isCopy = StringUtils.trimToEmpty(request.getParameter("isCopy"));
		
		if(newMenuImgFile != null && !newMenuImgFileName.equals("")){
			webHelpService.deleteOriginalFile(request, SHOP_UPLOAD_PATH, menuImgFileName);
			webHelpService.createNewUploadFile(request, SHOP_UPLOAD_PATH, newMenuImgFile, newMenuImgFileName);
			//
			shopModel.setMenuImgFileName(newMenuImgFileName);
		} else if (menuImgFileDeleteFlag != null && !menuImgFileDeleteFlag.equals("")) {
			webHelpService.deleteOriginalFile(request, SHOP_UPLOAD_PATH, menuImgFileName);
			shopModel.setMenuImgFileName("");
		}
		
		String smallLogImgFileName = request.getParameter("smallLogImgFileName");
		MultipartFile newsmallLogImgFile = request.getFile("newSmallLogImgFile");
		String newSmallLogImgFileName = newsmallLogImgFile.getOriginalFilename();
		String smallLogImgFileDeleteFlag = request.getParameter("smallLogImgFileDeleteFlag");
		
		if(newsmallLogImgFile != null && !newSmallLogImgFileName.equals("")){
			webHelpService.deleteOriginalFile(request, SHOP_UPLOAD_PATH, smallLogImgFileName);
			webHelpService.createNewUploadFile(request, SHOP_UPLOAD_PATH, newsmallLogImgFile, newSmallLogImgFileName);
			//
			shopModel.setSmallLogImgFileName(newSmallLogImgFileName);
		} else if (smallLogImgFileDeleteFlag != null && !smallLogImgFileDeleteFlag.equals("")) {
			webHelpService.deleteOriginalFile(request, SHOP_UPLOAD_PATH, smallLogImgFileName);
			shopModel.setSmallLogImgFileName("");
		}
		
		if (isCopy.equals("Y")) {
			service.write(shopModel);
		} else {
			service.modify(shopModel);
		}
		
		ModelAndView mav = new ModelAndView();
		mav.addObject("shopCode", shopModel.getShopCode());
		mav.setViewName("redirect:/shop/view.do");
		return mav;
	}
	
	@RequestMapping("/delete.do")
	public ModelAndView shopDelete(HttpServletRequest request, HttpSession session){
		String userId = (String) session.getAttribute("userId");
		String shopCode = request.getParameter("shop_code");
		
		ShopModel shop = service.getOne(shopCode);
		
		ModelAndView mav = new ModelAndView();
		
		if(!userId.equals(shop.getWriterid())){
			mav.addObject("errCode", "1");	// it's forbidden connection
			mav.addObject("shopCode", shopCode);
			mav.setViewName("redirect:view.do");
		} else {

			// if: when the article has upload file - remove that
			if(shop.getSmallLogImgFileName() != null){
				webHelpService.deleteOriginalFile(request, SHOP_UPLOAD_PATH, shop.getSmallLogImgFileName());
			}
			if(shop.getMenuImgFileName() != null){
				webHelpService.deleteOriginalFile(request, SHOP_UPLOAD_PATH, shop.getMenuImgFileName());
			}
			//				
			service.delete(shopCode);
			
			mav.setViewName("redirect:list.do");
		}		
		return mav;
	}
	
	@RequestMapping("/recommend.do")
	public ModelAndView updateRecommendcount(HttpServletRequest request, HttpSession session){
		String shopCode = request.getParameter("shop_code");
		String userId = (String) session.getAttribute("userId");
		ShopModel shop = service.getOne(shopCode);
		
		ModelAndView mav = new ModelAndView();
		
		/*
		if(userId.equals(shop.getWriterId())){
			mav.addObject("errCode", "1");
		} else {
			shopService.updateRecommendCount(shop.getRecommendcount()+1, idx);
		}
		*/
		mav.addObject("shopCode", shop.getShopCode());
		mav.setViewName("redirect:/shop/view.do");
		
		return mav;
	}
	
	@RequestMapping("/getAddress.do")
	@ResponseBody
	public String getAddress(HttpServletRequest request) {

		String responseText = "";
		String query = StringUtils.trimToEmpty(request.getParameter("query")).replaceAll(" ", "");
		String coord = StringUtils.trimToEmpty(request.getParameter("coord"));
		URL url;
		try {
			url = new URL("http://openapi.map.naver.com/api/geocode.php?key=4e39692c552db937b6e8e0ddd8148ead&encoding=utf-8&coord="+coord+"&query="+ query);
			logger.debug(url);
			String xmlString = httpHelpService.getXMLStringFromHttpRequest(url);
			Document doc = xmlHelpService.loadXMLFromString(xmlString);
			if (doc.getElementsByTagName("x").getLength() > 0) {
				String geoCodeX = doc.getElementsByTagName("x").item(0).getTextContent();
				String geoCodeY = doc.getElementsByTagName("y").item(0).getTextContent();
				responseText = geoCodeX + ","+geoCodeY;
			} else {
				//responseText = "{result:\"error\"}";
				responseText = "error";
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return responseText;
	}
}
