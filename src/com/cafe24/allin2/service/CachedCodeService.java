package com.cafe24.allin2.service;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

import com.cafe24.allin2.model.CodeModel;

public class CachedCodeService {
	// DI
	private ApplicationContext context = new GenericXmlApplicationContext("/config/applicationContext.xml");
	private CodeService service = (CodeService) context.getBean("codeService");

	Logger logger = Logger.getLogger(CachedCodeService.class);
	private Map<String,List<CodeModel>> codeListMap = new HashMap<String, List<CodeModel>>();
	
	public List<CodeModel> getCodeList(String codeName) {
		List<CodeModel> codeList = (List<CodeModel>)codeListMap.get(codeName);
		if (codeList != null) {
			logger.debug("caching");
			return codeList;
			
		} else {
			logger.debug("최초로 가져온다.");
			codeList = (List<CodeModel>)service.getCodeList(codeName);
			codeListMap.put(codeName, codeList);
		}
		
		return codeList;
	}
	
}
