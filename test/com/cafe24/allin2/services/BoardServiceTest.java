package com.cafe24.allin2.services;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

import com.cafe24.allin2.model.BoardModel;
import com.cafe24.allin2.service.BoardService;

public class BoardServiceTest {

	private static BoardService service;

	static Logger logger = Logger.getLogger(BoardService.class);
			
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ApplicationContext context = new GenericXmlApplicationContext("/config/applicationContext.xml");
		service = (BoardService) context.getBean("boardService");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}


	@Test
	public void testGetList() {
		
		List<BoardModel> banners = service.getList(1, 2, BoardModel.DATE_FORMAT1);
		
		System.out.println("banners > " + banners);
	}
	
	@Test
	public void testSearch() {
		
		Map<String,String> searchMap = new HashMap<String,String>();
		searchMap.put("type", "writer");
		searchMap.put("keyword", "김현규");
		
		List<BoardModel> banners = service.search(searchMap, 1, 2, BoardModel.DATE_FORMAT2, "AA", "공지사항");
		
		System.out.println("banners > " + banners);
	}
	
	
	@Test
	public void testGetSimpleNotice() {
		BoardModel board = service.getOneArticle(1);
		logger.debug(board.toString());
	}

}
