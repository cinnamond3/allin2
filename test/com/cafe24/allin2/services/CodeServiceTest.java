package com.cafe24.allin2.services;

import static org.junit.Assert.*;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

import com.cafe24.allin2.model.CodeModel;
import com.cafe24.allin2.service.CodeService;
import com.cafe24.allin2.service.ShopService;

public class CodeServiceTest {

	private static CodeService service;

	static Logger logger = Logger.getLogger(ShopServiceTest.class);
			
	@BeforeClass
	public static void setUpBeforeClass() {
		ApplicationContext context = new GenericXmlApplicationContext("/config/applicationContext.xml");
		service = (CodeService) context.getBean("codeService");
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetCodeList() {
		//given
		String codeName = "AREA";

		//when
		List<CodeModel> codes = service.getCodeList(codeName);
		
		assertEquals("[CodeModel [code_name=null, code_value=AA, code_desc=���, writeDate=null], CodeModel [code_name=null, code_value=AB, code_desc=�ƻ�, writeDate=null]]", codes.toString());
	}

}
