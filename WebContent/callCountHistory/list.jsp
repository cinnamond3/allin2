<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>전화 히스토리 목록 보기</title>
<link href="<%=request.getContextPath()%>/css/common.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript">
<!--
	function selectedOptionCheck(){
		$("#type > option[value=<%=request.getParameter("type")%>]").attr("selected", "true");
	}
	
	function view(index){
	    location.href = "view.do?idx="+index;
	}

	function moveAction(where){
		switch (where) {
		case 1:
			location.href = "write.do";
			break;
		
		case 2:
			location.href = "list.do";
			break;
		}
	}
//-->
</script>
</head>
<body onload="selectedOptionCheck()">
<div class="wrapper">
	<div class="nav">
		<jsp:include page="/common/menu.jsp"></jsp:include>
	</div>
	<div class="header">
		<form action="list.do" method="get">		
			시작일 : <input type="text" id="startDate" name="startDate" value="<%=StringUtils.trimToEmpty(request.getParameter("startDate"))%>" />
			~
			종료일 : <input type="text" id="endDate" name="endDate" value="<%=StringUtils.trimToEmpty(request.getParameter("endDate"))%>" />
			<input type="submit" value="검색" />
			<br/>
            * 날짜 형식은 YYYY-MM-DD 형식으로 입력하세요 예) 2013-08-26
		</form>
	</div>	
	
	<table border="0" class="view">
	   <colgroup>
	       <col width="10%"/>
	       <col width="10%"/>
	       <col width="40%"/>
	       <col width="10%"/>
	       <col width="10%"/>
	       <col width="20%"/>
	   </colgroup>
		<thead>
		<tr>
			<th>번호</th>
			<th>업체코드</th>
			<th>업체명</th>
			<th>전화번호</th>
			<th>배너</th>
			<th>작성일</th>	
		</tr>
		</thead>
		<tbody>
		<c:forEach var="callCountHistoryItem" items="${callCountHistoryList}">
		<tr>
			<td class="idx">${callCountHistoryItem.idx}</td>
			<td>${callCountHistoryItem.shopCode}</td>
			<td>${callCountHistoryItem.shopName}</td>
			<td>${callCountHistoryItem.phone}</td>
			<td>${callCountHistoryItem.bannerFlg}</td>
			<td>${callCountHistoryItem.writeDate}</td>		
		</tr>
		</c:forEach>
		</tbody>
	</table>
	<br />
	${pageHtml}
	<br /><br />
	<input type="button" value="목록" class="writeBt" onclick="moveAction(2)"/>
	<input type="button" value="쓰기" class="writeBt" onclick="moveAction(1)"/>	
</div>
</body>
</html>