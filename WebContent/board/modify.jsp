<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>글 수정: ${board.subject}</title>
<link href="<%=request.getContextPath()%>/css/board.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.7.1.js"></script>
<script type="text/javascript">
	function writeFormCheck() {
		if($("#subject").val() == null || $("#subject").val() == ""){
			alert("제목을 입력해 주세요!");
			$("#subject").focus();
			return false;
		}
		
		if($("#content").val() == null || $("#content").val() == ""){
			alert("내용을 입력해 주세요!");
			$("#content").focus();
			return false;
		}
		
		return true;
	}
	
	function moveAction(where){
		switch (where) {
		case 1:
			location.href ="list.do";
			break;
		}
	}
</script>
</head>
<body>
<div class="wrapper">
    <div class="nav">
        <jsp:include page="/common/menu.jsp"></jsp:include>
    </div>  	
	<h3>글 수정</h3>
	<form action="modify.do" method="post" onsubmit="return writeFormCheck()" enctype="multipart/form-data">	
	<table class="boardWrite">
        <colgroup>
            <col width="15%">
            <col width="35%">
            <col width="15%">
            <col width="35%">
        </colgroup>
            
		<tr>
		    <th><label for="header">헤더</label></th>
		    <td>
                <select id="header" name=header>
                    <c:forEach var="headerItem" items="${headerList}">
                        <option value="<c:out value="${headerItem.code_value }" />" 
                            <c:if test="${headerItem.code_value == board.header}">selected="selected"</c:if>>
                            <c:out value="${headerItem.code_desc }" />
                        </option>
                    </c:forEach>
                </select>
            </td>
            <th><label for="areaCode">지역</label></th>
            <td>
                <select id="areaCode" name=areaCode>
	                <c:forEach var="area" items="${areaList}">
			            <option value="<c:out value="${area.code_value }" />" 
			                <c:if test="${area.code_value == board.areaCode}">selected="selected"</c:if>>
			                <c:out value="${area.code_desc }" />
			            </option>
			        </c:forEach>
		        </select>
            </td>
        </tr>
		<tr>
			<th>제목</th>
			<td colspan="3">
				<input type="text" id="subject" name="subject" class="boardSubject" value="${board.subject}"/>
				<input type="hidden" id="writer" name="writer" value="${userName}" />
				<input type="hidden" id="writerId" name="writerId" value="${userId}" />
				<input type="hidden" id="idx" name="idx" value="${board.idx}" />
			</td>			
		</tr>
		<tr>
			<th>내용</th>
			<td colspan="3"><textarea id="content" name="content" class="boardContent">${board.content}</textarea></td>			
		</tr>
		<tr>
			<th><label for="file">파일</label></th>
			<td colspan="3">
				<input type="file" id="newFile" name="newFile" />
				<p>업로드된 파일: ${board.fileName}
				    <input type="hidden" id="orgFile" name="orgFile" value="${board.fileName}" />
				    <input type="checkbox" id="deleteFlag" name="deleteFlag" />파일삭제시 체크
				</p>
			</td>			
		</tr>				
	</table>
	<br />
	<input type="reset" value="재작성" class="writeBt"/>
	<input type="submit" value="확인" class="writeBt"/>	
	<input type="button" value="목록" class="writeBt" onclick="moveAction(1)" />
	</form>
</div>
</body>
</html>