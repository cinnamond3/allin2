package com.cafe24.allin2.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.cafe24.allin2.dao.ShopCallDao;
import com.cafe24.allin2.model.ShopCallModel;

public class ShopCallService extends SqlMapClientDaoSupport implements ShopCallDao {

	private HashMap<String, Object> valueMap = new HashMap<String, Object>();

	@Override
	public List<ShopCallModel> search(Map<String,String> searchMap, int startArticleNum, int endArticleNum) {
		valueMap.putAll(searchMap);
		valueMap.put("startNum", startArticleNum);
		valueMap.put("endNum", endArticleNum);
		return getSqlMapClientTemplate().queryForList("shopCall.search", valueMap);
	}
	
	@Override
	public int getSearchTotalNum(Map<String,String> searchMap) {
		valueMap.putAll(searchMap);
		return (Integer) getSqlMapClientTemplate().queryForObject("shopCall.getSearchTotalNum", valueMap);
	}

}
