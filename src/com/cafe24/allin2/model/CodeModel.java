package com.cafe24.allin2.model;

public class CodeModel {
	 
	private String code_name;
	private String code_value;
	private String code_desc;
	private String writeDate;
	 
	public String getCode_name() {
		return code_name;
	}  
	public void setCode_name(String code_name) {
		this.code_name = code_name;
	}
	public String getCode_value() {
		return code_value;
	}
	public void setCode_value(String code_value) {
		this.code_value = code_value;
	}
	public String getCode_desc() {
		return code_desc;
	}
	public void setCode_desc(String code_desc) {
		this.code_desc = code_desc;
	}	
	 
	public String getWriteDate() {
		return writeDate;
	}
	public void setWriteDate(String writeDate) {
		this.writeDate = writeDate;
	}
	@Override
	public String toString() {
		return "CodeModel [code_name=" + code_name + ", code_value="
				+ code_value + ", code_desc=" + code_desc + ", writeDate="
				+ writeDate + "]";
	}
}
