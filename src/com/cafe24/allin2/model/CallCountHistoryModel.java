package com.cafe24.allin2.model;

public class CallCountHistoryModel {
	
	private int rnum;
	private int idx;
	private String shopCode;
	private String shopName;
	private String phone;
	private String bannerFlg;
	private String writeDate;
	public int getRnum() {
		return rnum;
	}
	public void setRnum(int rnum) {
		this.rnum = rnum;
	}
	public int getIdx() {
		return idx;
	}
	public void setIdx(int idx) {
		this.idx = idx;
	}
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getBannerFlg() {
		return bannerFlg;
	}
	public void setBannerFlg(String bannerFlg) {
		this.bannerFlg = bannerFlg;
	}
	public String getWriteDate() {
		return writeDate;
	}
	public void setWriteDate(String writeDate) {
		this.writeDate = writeDate;
	}
	
	
}
