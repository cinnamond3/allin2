package com.cafe24.allin2.helpservice;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class HttpHelpService {
	
	Logger logger = Logger.getLogger(HttpHelpService.class);

	

	public String getXMLStringFromHttpRequest(URL url) {

		HttpURLConnection connection = null;
		String xmlString = "";

		try {
			// 요청 URL (예를 들어 제 홈페이지로)
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setConnectTimeout(3000);
			connection.setReadTimeout(3000);
			// 컨텐츠의 캐릭터셋이 euc-kr 이라면 (connection.getInputStream(), "euc-kr")
			BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream(), "utf8"));
			StringBuffer buffer = new StringBuffer();
			int read = 0;
			char[] cbuff = new char[1024];
			while ((read = reader.read(cbuff)) > 0) {
				buffer.append(cbuff, 0, read);
			}
			reader.close();
			xmlString = buffer.toString();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				connection.disconnect();
			}
		}
		return xmlString;
	}
	

}
